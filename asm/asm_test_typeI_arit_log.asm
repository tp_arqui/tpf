start:
	ADDI	R1, R0, 5   /*Result: 5*/
	ANDI	R3, R2, 2	/*Result: 2*/
	ANDI	R4, R2, 1   /*Result: 0*/
	ORI		R5, R2, 1	/*Result: 3*/
	XORI	R6, R2, 2   /*Result: 0*/
	LUI		R7, 1 		/*Result: 00000000000000001000000000000000 o 65536*/
	SLTI	R8, R8, 8	/*Result: 0*/
	halt
	