start:
	LB 		R1, 0(R8)		//R0=byte(memory[2]) con ext sig
	ADDU 	R31, R25, R25
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	LH 		R1, 0(R8)		//R1=half(memory[2]) con ext sig     
	XOR		R0, R1, R1      //para poner R0=0
	ADDU 	R31, R25, R25
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	LW   	R2, 1(R7)		//R2=word(memory[2]) con ext sig
	ADDU 	R31, R25, R25
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	LBU		R3, 0(R8)		//R3=byte(memory[2]) sin ext sig
	ADDU 	R31, R25, R25
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	LHU		R4, 1(R7)		//R4=half(memory[2]) sin ext sig
	ADDU 	R31, R25, R25
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	LWU		R5, 0(R8)		//R5=word(memory[2]) sin ext sig
	ADDI 	R31, R0, 65518   //hFFEE
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	SB		R31, 24(R0)	//memory[6] = R31[7:0]
	ADDU 	R31, R31, R0
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	SH		R31, 28(R0)	//memory[7] = R31[15:0]
	ADDU 	R31, R31, R0
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	SW		R31, 32(R0)	//memory[8] = R31[31:0]
	halt
	
