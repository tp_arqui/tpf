start:
	
	ADDU 	R1, R5, R4	//R0=9
	SUBU 	R1, R1, R2  //R0=-1
	AND 	R1, R23, R23 //R0=23
	OR 		R1, R1, R2  //R0=3
	XOR 	R1, R4, R5  //R0=1
	NOR 	R1, R1, R2  //R0=-4    ó    11111111111111111111111111111100
	SLT 	R1, R1, R20  //R0=1
	
	ADDI 	R1, R1, 7 	//R0=8
	ANDI 	R1, R23, 23  //R0=23
	ORI	 	R1, R1, 2   //R0=3
	XORI 	R1, R4, 5   //R0=1
	LUI 	R1, 10 	    //R0= h000a0000   ó 655360
	SLTI 	R1, R1, 2   //R0=1
	halt
	
