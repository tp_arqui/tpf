main:
	ADDI R1, R1, 1
	ADDI R2, R1, 4      	//R2=6 | si R2=3-> toma otro camino
	ADDI R3, R1, 4 			//R3=6
	BEQ  R2, R3, Label1 	//toma el branch a Label1
	ADDI R4, R0, 4 			//no ejecuta
	J Label2 				//no ejecuta
Label1:
	ADDI R4, R0, 3			//R4=3
	BNE  R3, R4, Label2 	//R4!=R3 -> salta a Label2
	ADDI R4, R0, 2          //no ejecuta
	ADDI R4, R0, 1          //no ejecuta
Label2: 
	ADDI R5, R0, 16			//R5=16
	JR   R5 				//jump a 16 -> HALT
	ADDI R4, R0, 2          //no ejecuta
	ADDI R4, R0, 0          //no ejecuta
Label3:
	JAL final 				//no ejecuta
	ADDI R5, R0, 16			//no ejecuta
final:
	halt 					//PC frenado
	
