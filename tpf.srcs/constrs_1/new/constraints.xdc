
create_clock -period 13.000 -name i_clock -waveform {0.000 6.500} [get_ports {i_clk i_enable}]

create_clock -period 10.000 -name i_clock -waveform {0.000 5.000} [get_ports i_clk]
