`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/21/2021 03:21:00 PM
// Design Name: 
// Module Name: EX_MEM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module EX_MEM
#(
	parameter               NB_INST         =   32,         // Longitud del registro
    parameter				NB_TREG		   	=   5,	   		// Longitud del campo RS,RT,RD
    parameter               NB_2            =   2
)
(
	// INPUTS
	input wire					           	i_clk,
    input wire					           	i_reset,
    input wire                              i_enable,
    input wire [NB_INST-1:0]               	i_ALUResult,
    input wire [NB_INST-1:0]               	i_reg_b,
    input wire [NB_TREG-1:0]				i_wreg_addr,

    // MEM
    input wire  							i_MemRead,
    input wire  							i_MemWrite,
    input wire  							i_Signed,
    input wire 	[NB_2-1:0]					i_Long,

    // WB
    input wire 	[NB_2-1:0]					i_MemtoReg,
    input wire  							i_RegWrite,
    input wire 	[NB_INST-1:0]				i_pc,
    
    // OUTPUTS
	output wire [NB_INST-1:0]               o_ALUResult,
    output wire [NB_INST-1:0]               o_reg_b,
    output wire [NB_TREG-1:0]				o_wreg_addr,


    // MEM
    output wire  							o_MemRead,
    output wire  							o_MemWrite,
    output wire  							o_Signed,
    output wire [NB_2-1:0]					o_Long,

    // WB
    output wire [NB_2-1:0]					o_MemtoReg,
    output wire  							o_RegWrite,
    output wire [NB_INST-1:0]				o_pc
);

	// INTERNAL
    reg [NB_INST-1:0]           ALUResult,	ALUResult_next;
    reg [NB_INST-1:0]           reg_b,		reg_b_next;
    reg [NB_TREG-1:0]			wreg_addr,	wreg_addr_next;

    // MEM
    reg  						MemRead,	MemRead_next;
    reg  						MemWrite,	MemWrite_next;
    reg  						Signed,		Signed_next;
    reg [NB_2-1:0]				Long,		Long_next;

    // WB
    reg [NB_2-1:0]				MemtoReg,	MemtoReg_next;
    reg  						RegWrite,	RegWrite_next;
    reg [NB_INST-1:0]			pc,			pc_next;

	always @(posedge i_clk) begin 
		if(i_reset) begin
			ALUResult	<= {NB_INST{1'b0}};
			reg_b 		<= {NB_INST{1'b0}};
		    wreg_addr	<= {NB_TREG{1'b0}};
		    MemRead 	<= 1'b0;
			MemWrite 	<= 1'b0;
			Signed 		<= 1'b0;
			Long 		<= {NB_2{1'b0}};
			MemtoReg 	<= {NB_2{1'b0}};
		    RegWrite 	<= 1'b0;
		    pc 			<= {NB_INST{1'b0}};
		end else begin                    //Funcionamiento normal del latch EX_MEM
			if (i_enable) begin
				ALUResult	<= ALUResult_next;
				reg_b 		<= reg_b_next;
			    wreg_addr	<= wreg_addr_next;
			    MemRead 	<= MemRead_next;
				MemWrite 	<= MemWrite_next;
				Signed 		<= Signed_next;
				Long 		<= Long_next;
				MemtoReg 	<= MemtoReg_next;
			    RegWrite 	<= RegWrite_next;
			    pc 			<= pc_next;
		    end
		end
	end

	always @(*) begin
		ALUResult_next	<= i_ALUResult;
		reg_b_next 		<= i_reg_b;
	    wreg_addr_next	<= i_wreg_addr;
	    MemRead_next 	<= i_MemRead;
		MemWrite_next 	<= i_MemWrite;
		Signed_next 	<= i_Signed;
		Long_next 		<= i_Long;
		MemtoReg_next 	<= i_MemtoReg;
	    RegWrite_next 	<= i_RegWrite;
	    pc_next			<= i_pc;	
    end
        
    // OUTPUT
    assign o_ALUResult	= ALUResult;
	assign o_reg_b 		= reg_b;
	assign o_wreg_addr	= wreg_addr;
    assign o_MemRead 	= MemRead;
	assign o_MemWrite 	= MemWrite;
	assign o_Signed 	= Signed;
	assign o_Long 		= Long;
	assign o_MemtoReg 	= MemtoReg;
    assign o_RegWrite 	= RegWrite;
    assign o_pc 		= pc;

endmodule