`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/11/2021 06:41:26 PM
// Design Name: 
// Module Name: tb_mips_timing
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_mips_timing();

	localparam NB_INST		= 	32;     	// Tama??o de instruccion
	localparam NB_ADDR 		= 	26;			// Longitud de la dir. inmediata (J|JAL)
    localparam NB_INMED		=   16;         // Longitud del inmediato
    localparam RAM_SIZE		= 	10;    		// Tamanio de la memoria, 1 MB
    localparam NB_OPCODE	=	6;			// Longitud del OPCODE
    localparam NB_TREG		=   5;	   		// Longitud del campo RS,RT,RD
    localparam NB_OPER		=   4;			// Longitud del operando de ALU
    localparam NB_ALUOP		=	3;			// Longitud del ALUOP
    localparam NB_2         =   2;
	localparam SB_TICK      =   16;
	localparam MEM_SIZE     =   5;
	localparam MEMORY_FILE  =   "arit_logic.mem";
	localparam STEPS        =   5;

	// TB_SIGNALS_inputs
    reg                         clk;
    wire                        i_clk;
    reg                         i_reset;
    // CLK_WIZ
    reg                         clk_reset;
    wire                        locked;

    // INPUTS
    reg                         step_by_step;
    reg							o_write_mips;
    reg                         i_btn;
    reg [NB_INST-1:0]          	instruction;
    reg [NB_INST-1:0]     		address;

    wire [NB_INST-1:0]     		o_result;

	initial begin
        #10
        clk 		        = 1'b0;
        i_reset             = 1'b1;
        clk_reset           = 1'b0;
        step_by_step        = 1'b0;
        i_btn               = 1'b0;
        o_write_mips		= 1'b0;
        address 			= 1'b0;
        instruction 		= 1'b0;

        #10
        clk_reset           = 1'b1;

        while(~locked) begin
            #10
            clk_reset = 1'b0;
        end
       
        #10
        i_reset 	       = 1'b1;

        #30
        i_reset 	       = 1'b0;
        
        #60
        i_btn = 1'b1;
            
        #100
        i_btn = 1'b0;
        #60
        i_btn = 1'b1;
            
        #100
        i_btn = 1'b0;
        #60
        i_btn = 1'b1;
            
        #200
        i_btn = 1'b0;
        #120
        i_btn = 1'b1;
            
        #100
        i_btn = 1'b0;
        #60
        i_btn = 1'b1;
            
        #200
        i_btn = 1'b0;
        #120
        i_btn = 1'b1;
            
        #100
        i_btn = 1'b0;
        #60
        i_btn = 1'b1;
            
        #100
        i_btn = 1'b0;
        #120
        i_btn = 1'b1;
            
        #200
        i_btn = 1'b0;
        $display("############# Test OK ############");
        $finish();

    end

    // CLOCK_GENERATION
    always #20 clk = ~clk;

    mips
    #(
    	.NB_INST     		(NB_INST),
       	.NB_ADDR          	(NB_ADDR),
       	.NB_INMED	        (NB_INMED),
       	.RAM_SIZE			(RAM_SIZE),
       	.NB_OPCODE        	(NB_OPCODE),
       	.NB_TREG        	(NB_TREG),
       	.NB_OPER        	(NB_OPER),
       	.NB_ALUOP           (NB_ALUOP),
       	.NB_2           	(NB_2)
    )
    u_mips
    (
    	.i_clk				(i_clk),
    	.i_reset			(i_reset),
    	.i_write			(o_write_mips),
    	.i_enable			(locked),
    	.i_instruction		(instruction),
    	.i_address		    (address),
    	.o_result			(o_result)
	);
        
    clk_wiz_0 uut
    (
        .clk_out1           (i_clk),
        .reset              (clk_reset),
        .locked             (locked),
        .clk_in1            (clk)        
    );

endmodule