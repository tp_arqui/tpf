`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/15/2021 07:45:17 PM
// Design Name: 
// Module Name: EX_mux_ALUSrc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module EX_mux_ALUSrc
#(
    parameter               NB_INST         =   32           // Longitud de registro con signo
)
(
    // INPUTS   
    input wire  [NB_INST-1:0]               i_a,
    input wire  [NB_INST-1:0]               i_b,
    input wire                              i_sel,
    
    // OUTPUTS
    output wire [NB_INST-1:0]               o_mux
);

    // OUTPUT
    assign o_mux = (i_sel) ? i_b : i_a;                     // (i_sel==1)->signext | (i_sel==0)->RegB
    
endmodule