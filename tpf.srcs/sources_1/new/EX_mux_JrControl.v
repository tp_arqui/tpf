`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/18/2021 07:40:32 PM
// Design Name: 
// Module Name: EX_mux_JrControl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module EX_mux_JrControl
#(
    parameter               NB_INST         =   32           // Longitud de registro con signo
)
(
    // INPUTS   
    input wire  [NB_INST-1:0]               i_a,
    input wire  [NB_INST-1:0]               i_b,
    input wire                              i_sel,
    
    // OUTPUTS
    output wire [NB_INST-1:0]               o_mux
);

    // OUTPUT
    assign o_mux = (i_sel) ? i_b : i_a;                     // (i_sel==1)->PC+4 | (i_sel==0)->RegA
    
endmodule
