`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/05/2021 08:37:23 AM
// Design Name: 
// Module Name: step
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module step
(
    // INPUTS
    input wire                  i_clk,
    input wire                  i_reset,
    input wire                  i_btn,
    
    // OUTPUTS
    output wire 	             o_step
);
    localparam                  ON  = 1'b1;
    localparam                  OFF = 1'b0;      
    
    // INTERNAL
    reg                         state_reg, state_next = OFF;
    reg                         clk, clk_next;

    always @(*) begin
        if (i_btn) state_next <= ON;
        else state_next <= OFF;          
    end
    
    always @(posedge i_clk) begin
        case(state_reg)
            ON:
                if (i_btn) clk_next <= OFF;
                else clk_next <= OFF;
            OFF:
                if (i_btn) clk_next <= ON;
                else clk_next <= OFF;
            default: clk_next <= OFF;
        endcase 
    end

    always @(posedge i_clk) begin
        if(i_reset) begin
            state_reg   <= OFF;
            clk         <= OFF;
        end else begin
            state_reg   <= state_next;
            clk         <= clk_next;
        end
    end
     
    // OUTPUT
    assign o_step = clk;
    
endmodule
