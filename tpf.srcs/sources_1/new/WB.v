`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/21/2021 07:45:17 AM
// Design Name: 
// Module Name: WB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module WB
#(
    parameter               NB_INST         =   32,         // Longitud de registro
    parameter               NB_TREG         =   5,          // Longitud del campo RT,RD y Shamt
    parameter               NB_2            =   2           // Longitud del selector
)
(
    // INPUTS   
    input wire  [NB_TREG-1:0]               i_wreg_addr,
    input wire                              i_RegWrite,
    input wire  [NB_2-1:0]                  i_MemtoReg,
    input wire  [NB_INST-1:0]               i_pc,
    input wire  [NB_INST-1:0]               i_DataRead,
    input wire  [NB_INST-1:0]               i_ALUResult,
    
    // OUTPUTS
    output wire [NB_TREG-1:0]               o_wreg_addr,
    output wire                             o_RegWrite,
    output wire [NB_INST-1:0]               o_wreg_data
);

    // OUTPUT
    assign o_wreg_addr      = i_wreg_addr;
    assign o_RegWrite       = i_RegWrite;

    WB_mux
    #(
        .NB_INST            (NB_INST),
        .NB_SEL             (NB_2)
    )
    u_WB_mux
    (
        .i_a                (i_ALUResult),
        .i_b                (i_DataRead),
        .i_c                (i_pc),
        .i_sel              (i_MemtoReg),
        .o_mux              (o_wreg_data)
    );
    
endmodule