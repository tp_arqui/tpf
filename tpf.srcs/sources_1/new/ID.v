`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/05/2021 07:32:38 PM
// Design Name: 
// Module Name: ID
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ID
#(
	parameter               NB_INST         =   32,         // Longitud con signo
    parameter				NB_ADDR 		= 	26,			// Longitud de la dir. inmediata (J|JAL)
    parameter               NB_INMED        =   16,         // Longitud del inmediato
    parameter				NB_OPCODE		=	6,			// Longitud del OPCODE
    parameter				NB_TREG		   	=   5,	   		// Longitud del campo RS,RT,RD
    parameter				NB_ALUOP		=	3,			// Longitud del ALUOP
    parameter               NB_2            =   2
)
(
	// INPUTS   
    input wire					           	i_clk,
    input wire					           	i_reset,

    input wire                              i_RegWrite,
    input wire  [NB_INST-1:0]				i_instruction,
    input wire  [NB_INST-1:0]				i_pc,		// Input: resultado del sumador de IF para calc. la dir. de salto

    input wire  [NB_TREG-1:0]           	i_wreg_addr,	// Input: direccion obtenida desde MUX en EX
    input wire  [NB_INST-1:0]	          	i_wreg_data,	// Input: dato obtenido desde WB
    input wire  [NB_2-1:0]                  i_forwardA_Branch,
    input wire  [NB_2-1:0]                  i_forwardB_Branch,

    input wire  [NB_INST-1:0]               i_reg_EX,
    input wire  [NB_INST-1:0]               i_reg_MEM,
    input wire  [NB_INST-1:0]               i_reg_WB,

    // OUTPUTS
    // FETCH
    output wire [NB_INST-1:0]				o_jump_addr,	// Output: direccion del salto calculada
    output wire [NB_INST-1:0]				o_branch_addr,	// Output: direccion del branch calculada
    output wire [NB_INST-1:0]				o_jr_addr,		// Output: direccion del salto leida de Reg A (JR|JALR)
    output wire								o_Branch,		// Output: senial de control para MPX Branch
    output wire								o_Jump,			// Output: senial de control para MPX Jump
    output wire								o_Jr,			// Output: senial de control para MPX Jr

    // EX
	output wire [NB_ALUOP-1:0]				o_ALUOp,		// Output: senial de control para modulo ALUControl y Comparador
	output wire 							o_ALUSrc,		// Output: senial de control para MPX ALUSrc
	output wire [NB_2-1:0]				    o_RegDst,		// Output: senial de control para MPX WriteRegister
	output wire [NB_INST-1:0]				o_reg_a,		// Output: valor del registro A
	output wire [NB_INST-1:0]				o_reg_b,		// Output: valor del registro B
	output wire [NB_INST-1:0]				o_signext,		// Output: inmed. con singo extendido para MPX ALUSrc
    output wire [NB_TREG-1:0]               o_shamt,        // Output: cantidad de bit a desplazar para ALUCtrl
    output wire [NB_TREG-1:0]               o_reg_rs,       // Output: direccion del registro RS para MPX RegDest
	output wire [NB_TREG-1:0]				o_reg_rt,		// Output: direccion del registro RT para MPX RegDest
	output wire [NB_TREG-1:0]				o_reg_rd,		// Output: direccion del registro RD para MPX RegDest

	// MEM
	output wire 							o_MemRead,		// Output: senial de control para mod. MEM
	output wire 							o_MemWrite,		// Output: senial de control para mod. MEM
	output wire 							o_Signed,		// Output: senial de control para mod. MEM (LOAD signado)
	output wire [NB_2-1:0]					o_Long,			// Output: senial de control para mod. MEM (Longitud de dato leido/escrito)

	// WB
	output wire [NB_2-1:0]				    o_MemtoReg,		// Output: senial de control para MPX final (MemtoReg)
    output wire                             o_RegWrite,     // Output: senial de control para banco de Reg en WB
    output wire [NB_INST-1:0]               o_pc,           // Output: contador de programa
    output wire                             o_Halt          // Output: senial de control para instruccion HALT
);

	// INTERNAL
	wire 					comp;					// Output comparador (BEQ|BNE)
	wire 					Branch;					// Senial de control para determinar la cond de salto BEQ
    wire                    Branchne;               // Senial de control para determinar la cond de salto BNE
    wire [NB_OPCODE-1:0]    opcode      = i_instruction[31:26];
    wire [NB_TREG-1:0]      rs          = i_instruction[25:21];
    wire [NB_TREG-1:0]      rt          = i_instruction[20:16];
    wire [NB_TREG-1:0]      rd          = i_instruction[15:11];
    wire [NB_TREG-1:0]      shamt       = i_instruction[10:6];
    wire [NB_OPCODE-1:0]    funct       = i_instruction[5:0];
    wire [NB_INMED-1:0]     inmed       = i_instruction[15:0];
    wire [NB_ADDR-1:0]      address     = i_instruction[25:0];
    wire [NB_INST-1:0]      regA;
    wire [NB_INST-1:0]      regB;

	// OUTPUT
	assign o_Branch = ((Branch && comp) || (Branchne && ~comp));
	assign o_jr_addr = o_reg_a;
	assign o_jump_addr = {i_pc[31:26], address} + 1'b1;    // Porque no estamos haciendo el <<2
    assign o_reg_rs = rs;
	assign o_reg_rt = rt;
	assign o_reg_rd = rd;
    assign o_shamt = shamt;
    assign o_pc = i_pc;

	ID_control
	#(
        .NB_OPCODE     		(NB_OPCODE),
        .NB_ALUOP 			(NB_ALUOP),
        .NB_2               (NB_2)
    )
    u_ID_control
    (
    	.i_reset			(i_reset),
    	.i_opcode 			(opcode),
    	.o_RegDst 			(o_RegDst),
    	.o_ALUSrc 			(o_ALUSrc),
    	.o_ALUOp 			(o_ALUOp),
    	.o_MemRead 			(o_MemRead),
    	.o_MemWrite 		(o_MemWrite),
    	.o_Branch 			(Branch),
        .o_Branchne         (Branchne),
    	.o_Signed 			(o_Signed),
    	.o_Long 			(o_Long),
    	.o_Jump 			(o_Jump),
    	.o_RegWrite 		(o_RegWrite),
    	.o_MemtoReg 		(o_MemtoReg),
        .o_Halt             (o_Halt)
    );

	ID_reg
	#(
        .NB_REG     		(NB_INST),
        .NB_TREG 			(NB_TREG)
    )
    u_ID_reg
    (
    	.i_clk				(i_clk),
    	.i_reset			(i_reset),
    	.i_RegWrite 		(i_RegWrite),
    	.i_reg_a 			(rs),
    	.i_reg_b 			(rt),
    	.i_wreg_addr 		(i_wreg_addr),
    	.i_wreg_data 		(i_wreg_data),
    	.o_reg_a			(regA),
    	.o_reg_b 			(regB)
    );

    ID_jrcontrol
	#(
        .NB_OPCODE     		(NB_OPCODE)
    )
    u_ID_jrcontrol
    (
        .i_reset            (i_reset),
        .i_funct           	(funct),
        .i_opcode           (opcode),
        .o_jrcontrol		(o_Jr)
    );

    ID_comp
    #(
        .NB_INST     		(NB_INST)
    )
    u_ID_comp
    (
        .i_a              	(o_reg_a),
        .i_b              	(o_reg_b),
        .o_comp 			(comp)
    );

	ID_sum
	#(
        .NB_INST     		(NB_INST)
    )
    u_ID_sum
    (
        .i_a              	(i_pc),
        .i_b              	(o_signext),
        .o_add 				(o_branch_addr)
    );

    ID_signext
    #(
        .NB_INST     		(NB_INST),
        .NB_INMED          	(NB_INMED),
        .NB_OPCODE          (NB_OPCODE)
    )
    u_ID_signext
    (
        .i_se              	(inmed),
        .i_opcode           (opcode),
        .o_se 				(o_signext)
    );

    ID_mux_A
    #(
        .NB_INST            (NB_INST),
        .NB_SEL             (NB_2)
    )
    u_ID_mux_A
    (
        .i_a                (regA),
        .i_b                (i_reg_EX),
        .i_c                (i_reg_MEM),
        .i_d                (i_reg_WB),
        .i_sel              (i_forwardA_Branch),
        .o_mux              (o_reg_a)
    );

    ID_mux_B
    #(
        .NB_INST            (NB_INST),
        .NB_SEL             (NB_2)
    )
    u_ID_mux_B
    (
        .i_a                (regB),
        .i_b                (i_reg_EX),
        .i_c                (i_reg_MEM),
        .i_d                (i_reg_WB),
        .i_sel              (i_forwardB_Branch),
        .o_mux              (o_reg_b)
    );

endmodule