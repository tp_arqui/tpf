`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/05/2021 07:33:46 PM
// Design Name: 
// Module Name: IF_ID
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module IF_ID
#
(   
    parameter               NB_INST         =   32,         // Longitud con signo
    parameter				NB_ADDR 		= 	32			
)
(
    //INPUTS
    input wire					           	i_clk,
    input wire					           	i_reset,
    input wire                              i_enable,
    input wire					           	i_stall,
    input wire					           	i_flush,
    input wire  [NB_INST-1:0]				i_instruction,        
    input wire  [NB_ADDR-1:0]				i_pc_sum,
    input wire                             	i_Halt,
  
    //OUTPUTS
    output wire  [NB_INST-1:0]				o_instruction,        
    output wire  [NB_ADDR-1:0]				o_pc_sum      
);
    
    // INTERNAL
    reg [NB_ADDR-1 :0] pc_sum, pc_sum_next;
	reg [NB_INST-1 :0] instruction, instruction_next;

	always @(posedge i_clk) begin 
		if(i_reset) begin
			pc_sum  <= 32'b0;
			instruction <= 32'b0;
		end else if (i_enable) begin
            if (i_stall || i_Halt) begin  //Se mantiene en la instruccion y la direccion actual
                pc_sum  <= pc_sum;
                instruction <= instruction;
    		end else if (i_flush) begin        //Se limpia el pipe
    			pc_sum  <= pc_sum;
    			instruction <= 32'b0;	
    		end else begin                    //Funcionamiento normal del latch IF_ID
    			pc_sum  <= pc_sum_next;
    			instruction <= instruction_next;
    		end
        end
	end

    always @(*) begin
        pc_sum_next = i_pc_sum;
        instruction_next = i_instruction;
    end
    
	assign o_instruction = instruction;
	assign o_pc_sum  = pc_sum;
endmodule
