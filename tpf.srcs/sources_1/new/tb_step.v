`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/05/2021 08:37:23 AM
// Design Name: 
// Module Name: tb_step
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module tb_step();

	// TB_SIGNALS_inputs
    reg                         clk;
    wire                        i_clk;
    reg                         i_reset;
    // CLK_WIZ
    reg                         clk_reset;
    wire                        locked;
    // STEP
    reg                         i_btn;
    wire                        o_step;

    initial begin
            #10
            clk         = 1'b0;
            i_reset     = 1'b1;
            clk_reset   = 1'b0;
            i_btn       = 1'b0;
    
            #10
            clk_reset   = 1'b1;
    
            while(~locked) begin
                #10
                clk_reset = 1'b0;
            end
            
            i_reset     = 1'b0;
            
            #60
            i_btn = 1'b1;
            
            #100
            i_btn = 1'b0;

    
       
            $display("############# Test OK ############");
            $finish();
    
        end
    
        // CLOCK_GENERATION
        always #20 clk = ~clk;
        
        clk_wiz_0 uut
        (
            .clk_out1           (i_clk),
            .reset              (clk_reset),
            .locked             (locked),
            .clk_in1            (clk)        
        );
        
        step u_step
        (
            .i_clk              (i_clk),
            .i_reset            (i_reset),
            .i_btn              (i_btn),
            .o_clk              (o_step)
        );
       
    
endmodule