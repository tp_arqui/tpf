`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/05/2021 07:45:17 PM
// Design Name: 
// Module Name: ID_signext
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ID_signext
#(
    parameter               NB_INST         =   32,         // Longitud con signo
    parameter               NB_INMED        =   16,         // Longitud sin signo
    parameter				NB_OPCODE		=	6			// Longitud del OPCODE
)
(
    // INPUTS   
    input wire  [NB_INMED-1:0]             i_se,
    input wire  [NB_OPCODE-1:0]            i_opcode,
    
    // OUTPUTS
	output wire [NB_INST-1:0]              o_se
);

	// LOCALPARAMETERS
	localparam 			inm = 6'b001???;
    
    // OUTPUT
    assign o_se = (i_opcode == inm) ? $unsigned(i_se) : $signed(i_se);
    
endmodule