`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/10/2021 04:26:09 PM
// Design Name: 
// Module Name: ID_jrcontrol
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ID_jrcontrol
#(
	parameter				NB_OPCODE		=	6 			// Longitud del FUNCT
)
(
	// INPUTS   
    input wire					           	i_reset,
    input wire	[NB_OPCODE-1:0]				i_funct,
    input wire 	[NB_OPCODE-1:0]				i_opcode,

    // OUTPUTS
    output wire                 			o_jrcontrol
);


	// LOCALPARAMETERS
	localparam 						jr  	  = 6'b001000;								
	localparam 						jalr	  = 6'b001001;
	localparam 						rtype	  = 6'b000000;

	// OUTPUT
	assign o_jrcontrol = ((i_opcode == rtype) && ((i_funct == jr) || (i_funct == jalr)));

endmodule