`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/28/2021 06:06:36 PM
// Design Name: 
// Module Name: tx_program
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tx_program
#(
   parameter MEMORY_FILE    = "arit_logic.mem",
   parameter SB_TICK		= 16,         // Ticks UART
   parameter NB_DATA        = 32,              //Tamaño de las instrucciones
   parameter MEM_SIZE       = 5               //Tamanio de la memoria de 32 instrucciones
)
(
    input  wire                            i_clk,
	input  wire                            i_reset,
	input  wire                            i_send_program,
	output  wire                           o_tx              //Salida serial 
);

    //INTERNAL
    reg        [NB_DATA-1:0]        memory[2**MEM_SIZE-1:0];            // Array de registros de 32 de largo y 32 de ancho
    reg        [NB_DATA-1:0]        addr=0, addr_next=0;                // la direccion inicial en 0
    //wire                            
    reg                             tx_start;
    reg         [NB_DATA-1:0]       instruction;
    
    initial begin
        $readmemb(MEMORY_FILE, memory, 0);                                 //Carga la memoria del programa a partir de la ruta especificada por DATA_FILE
    end 
    
    always@(posedge i_clk)
    begin : next_address
        if (i_reset) begin
           tx_start<=1;     ///Comienzo a transmitir en el momento que llega el reset
           addr<=0;
           instruction<=0;
        end
        else if (i_send_program && tx_done) begin   
            addr<=addr_next;
            instruction<=memory[addr];
            tx_start<=1;
        end
        else if(tx_start==1)
            tx_start<=0;    
    end
    
    
    
    always @(*)
        addr_next = addr+1;  //Se actualiza con el clk en el always de arriba
        
        
    uart
    #(
        .NB_DATA          (NB_DATA),
        .SB_TICK          (SB_TICK)
    )
    u_uart_tx
    (
        .i_clk            (i_clk),
        .i_reset          (i_reset), 
        .i_tx_start       (tx_start),
        .i_tx             (instruction),
        .o_tx             (o_tx),
        .o_tx_done        (tx_done)
    );
endmodule