`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/21/2021 03:21:00 PM
// Design Name: 
// Module Name: ID_EX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ID_EX
#(
	parameter               NB_INST         =   32,         // Longitud con signo
    parameter				NB_TREG		   	=   5,	   		// Longitud del campo RS,RT,RD
    parameter				NB_ALUOP		=	3,			// Longitud del ALUOP
    parameter               NB_2            =   2
)
(
	// INPUTS   
    input wire					           	i_clk,
    input wire					           	i_reset,
    input wire                              i_enable,
    input wire  							i_stall,
    // EX
	input wire [NB_ALUOP-1:0]				i_ALUOp,		// input: senial de control para modulo ALUControl y Comparador
	input wire 								i_ALUSrc,		// input: senial de control para MPX ALUSrc
	input wire [NB_2-1:0]				    i_RegDst,		// input: senial de control para MPX WriteRegister
	input wire [NB_INST-1:0]				i_reg_a,		// input: valor del registro A
	input wire [NB_INST-1:0]				i_reg_b,		// input: valor del registro B
	input wire [NB_INST-1:0]				i_signext,		// input: inmed. con singo extendido para MPX ALUSrc
    input wire [NB_TREG-1:0]               	i_shamt,        // input: cantidad de bit a desplazar para ALUCtrl
    input wire [NB_TREG-1:0]				i_reg_rs,		// input: direccion del registro RS para Forwarding Unit
	input wire [NB_TREG-1:0]				i_reg_rt,		// input: direccion del registro RT para MPX RegDest
	input wire [NB_TREG-1:0]				i_reg_rd,		// input: direccion del registro RD para MPX RegDest
    input wire								i_Jr,			// input: senial de control para MPX Jr

	// MEM
	input wire 								i_MemRead,		// input: senial de control para mod. MEM
	input wire 								i_MemWrite,		// input: senial de control para mod. MEM
	input wire 								i_Signed,		// input: senial de control para mod. MEM (LOAD signado)
	input wire [NB_2-1:0]					i_Long,			// input: senial de control para mod. MEM (Longitud de dato leido/escrito)

	// WB
	input wire [NB_2-1:0]				    i_MemtoReg,		// input: senial de control para MPX final (MemtoReg)
    input wire                             	i_RegWrite,     // input: senial de control para banco de Reg en WB
    input wire [NB_INST-1:0]               	i_pc,           // input: contador de programa

    // OUTPUTS
    output wire [NB_ALUOP-1:0]				o_ALUOp,		// output: senial de control para modulo ALUControl y Comparador
	output wire 							o_ALUSrc,		// output: senial de control para MPX ALUSrc
	output wire [NB_2-1:0]				    o_RegDst,		// output: senial de control para MPX WriteRegister
	output wire [NB_INST-1:0]				o_reg_a,		// output: valor del registro A
	output wire [NB_INST-1:0]				o_reg_b,		// output: valor del registro B
	output wire [NB_INST-1:0]				o_signext,		// output: inmed. con singo extendido para MPX ALUSrc
    output wire [NB_TREG-1:0]               o_shamt,        // output: cantidad de bit a desplazar para ALUCtrl
    output wire [NB_TREG-1:0]				o_reg_rs,		// output: direccion del registro RS para Forwarding Unit
	output wire [NB_TREG-1:0]				o_reg_rt,		// output: direccion del registro RT para MPX RegDest
	output wire [NB_TREG-1:0]				o_reg_rd,		// output: direccion del registro RD para MPX RegDest
    output wire								o_Jr,			// Output: senial de control para MPX Jr

	// MEM
	output wire 							o_MemRead,		// output: senial de control para mod. MEM
	output wire 							o_MemWrite,		// output: senial de control para mod. MEM
	output wire 							o_Signed,		// output: senial de control para mod. MEM (LOAD signado)
	output wire [NB_2-1:0]					o_Long,			// output: senial de control para mod. MEM (Longitud de dato leido/escrito)

	// WB
	output wire [NB_2-1:0]				    o_MemtoReg,		// output: senial de control para MPX final (MemtoReg)
    output wire                             o_RegWrite,     // output: senial de control para banco de Reg en WB
    output wire [NB_INST-1:0]               o_pc           	// output: contador de programa
);

	// INTERNAL
	// EX
	reg [NB_ALUOP-1:0]				ALUOp, 		ALUOp_next;
	reg 							ALUSrc, 	ALUSrc_next;
	reg [NB_2-1:0]				    RegDst, 	RegDst_next;
	reg [NB_INST-1:0]				reg_a, 		reg_a_next;
	reg [NB_INST-1:0]				reg_b,		reg_b_next;
	reg [NB_INST-1:0]				signext,	signext_next;
    reg [NB_TREG-1:0]               shamt, 		shamt_next;
    reg [NB_TREG-1:0]				reg_rs, 	reg_rs_next;
	reg [NB_TREG-1:0]				reg_rt, 	reg_rt_next;
	reg [NB_TREG-1:0]				reg_rd, 	reg_rd_next;
    reg								Jr, 		Jr_next;

	// MEM
	reg 							MemRead, 	MemRead_next;
	reg 							MemWrite,	MemWrite_next;
	reg 							Signed,	 	Signed_next;
	reg [NB_2-1:0]					Long,	 	Long_next;

	// WB
	reg [NB_2-1:0]				    MemtoReg,	MemtoReg_next;
    reg                             RegWrite,	RegWrite_next;
    reg [NB_INST-1:0]               pc,		 	pc_next;

	always @(posedge i_clk) begin 
		if(i_reset) begin
			ALUOp 		<= {NB_ALUOP{1'b0}};
			ALUSrc 		<= 1'b0;
			RegDst 		<= {NB_2{1'b0}};
			reg_a 		<= {NB_INST{1'b0}};
			reg_b 		<= {NB_INST{1'b0}};
			signext 	<= {NB_INST{1'b0}};
		    shamt 		<= {NB_TREG{1'b0}};
		    reg_rs		<= {NB_TREG{1'b0}};
			reg_rt 		<= {NB_TREG{1'b0}};
			reg_rd 		<= {NB_TREG{1'b0}};
		    Jr 			<= 1'b0;
		    MemRead 	<= 1'b0;
			MemWrite 	<= 1'b0;
			Signed 		<= 1'b0;
			Long 		<= {NB_2{1'b0}};
			MemtoReg 	<= {NB_2{1'b0}};
		    RegWrite 	<= 1'b0;
		    pc 			<= {NB_INST{1'b0}};
	    end else if (i_enable) begin
		    if (i_stall) begin
		    	ALUOp 		<= 3'b011;
				ALUSrc 		<= 1'b0;
				RegDst 		<= 2'b01;
				reg_a 		<= {NB_INST{1'b0}};
				reg_b 		<= {NB_INST{1'b0}};
				signext 	<= {NB_INST{1'b0}};
			    shamt 		<= {NB_TREG{1'b0}};
			    reg_rs		<= {NB_TREG{1'b0}};
				reg_rt 		<= {NB_TREG{1'b0}};
				reg_rd 		<= {NB_TREG{1'b0}};
			    Jr 			<= 1'b0;
			    MemRead 	<= 1'b0;
				MemWrite 	<= 1'b0;
				Signed 		<= 1'b0;
				Long 		<= {NB_2{1'b0}};
				MemtoReg 	<= {NB_2{1'b0}};
			    RegWrite 	<= 1'b0;
			    pc 			<= pc_next;
			end else begin                    //Funcionamiento normal del latch ID_EX
				ALUOp 		<= ALUOp_next;
				ALUSrc 		<= ALUSrc_next;
				RegDst 		<= RegDst_next;
				reg_a 		<= reg_a_next;
				reg_b 		<= reg_b_next;
				signext 	<= signext_next;
			    shamt 		<= shamt_next;
			    reg_rs 		<= reg_rs_next;
				reg_rt 		<= reg_rt_next;
				reg_rd 		<= reg_rd_next;
			    Jr 			<= Jr_next;
			    MemRead 	<= MemRead_next;
				MemWrite 	<= MemWrite_next;
				Signed 		<= Signed_next;
				Long 		<= Long_next;
				MemtoReg 	<= MemtoReg_next;
			    RegWrite 	<= RegWrite_next;
			    pc 			<= pc_next;
			end
		end
	end

	always @(*) begin
	    ALUOp_next 		<= i_ALUOp;
		ALUSrc_next		<= i_ALUSrc;
		RegDst_next		<= i_RegDst;
		reg_a_next 		<= i_reg_a;
		reg_b_next 		<= i_reg_b;
		signext_next 	<= i_signext;
	    shamt_next 		<= i_shamt;
	    reg_rs_next		<= i_reg_rs;
		reg_rt_next		<= i_reg_rt;
		reg_rd_next		<= i_reg_rd;
	    Jr_next			<= i_Jr;
	    MemRead_next 	<= i_MemRead;
		MemWrite_next 	<= i_MemWrite;
		Signed_next 	<= i_Signed;
		Long_next 		<= i_Long;
		MemtoReg_next 	<= i_MemtoReg;
	    RegWrite_next 	<= i_RegWrite;
	    pc_next			<= i_pc;	
    end

    // OUTPUT
    assign o_ALUOp 		= ALUOp;
	assign o_ALUSrc 	= ALUSrc;
	assign o_RegDst 	= RegDst;
	assign o_reg_a 		= reg_a;
	assign o_reg_b 		= reg_b;
	assign o_signext 	= signext;
    assign o_shamt 		= shamt;
	assign o_reg_rs 	= reg_rs;
	assign o_reg_rt 	= reg_rt;
	assign o_reg_rd 	= reg_rd;
    assign o_Jr 		= Jr;
    assign o_MemRead 	= MemRead;
	assign o_MemWrite 	= MemWrite;
	assign o_Signed 	= Signed;
	assign o_Long 		= Long;
	assign o_MemtoReg 	= MemtoReg;
    assign o_RegWrite 	= RegWrite;
    assign o_pc 		= pc;

endmodule