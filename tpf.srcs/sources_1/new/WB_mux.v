`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/21/2021 07:45:17 AM
// Design Name: 
// Module Name: WB_mux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module WB_mux
#(
    parameter               NB_INST         =   32,         // Longitud de registro
    parameter               NB_SEL          =   2           // Longitud del selector
)
(
    // INPUTS   
    input wire  [NB_INST-1:0]               i_a,
    input wire  [NB_INST-1:0]               i_b,
    input wire  [NB_INST-1:0]               i_c,
    input wire  [NB_SEL -1:0]               i_sel,
    
    // OUTPUTS
    output wire [NB_INST-1:0]               o_mux
);

    // LOCALPARAMETERS
    localparam                      alu = 2'b00;                                
    localparam                      mem = 2'b01;
    localparam                      pc  = 2'b10;
    localparam                      def = 5'b00000;
    
    // INTERNAL
    reg         [NB_INST-1:0]               mux;

    always @(*) begin : proc_mux
        case (i_sel)
            
            alu : mux <= i_a;        // WRegData -> ALUResult   
            mem : mux <= i_b;        // WRegData -> DataRead
            pc  : mux <= i_c+1;      // WRegData -> PC+2     
            default : mux <= def;    // WRegData -> 32'b0
        endcase
    end

    // OUTPUT
    assign o_mux = mux;
    
endmodule
