`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/07/2021 07:45:17 PM
// Design Name: 
// Module Name: IF_mux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module IF_rom
#(
    parameter MEMORY_FILE    = "arit_logic.mem",
    parameter NB_ADDR        = 32,             //Puedo direccionar hasta 4GB. 
    parameter NB_INST        = 32,              //Tamaño de las instrucciones
    parameter NB_ROM_SIZE    = 10               //Tamanio de la memoria de 1MB                      
) 
(
    input  wire                            i_clk,
    input  wire                            i_reset,
    input  wire                            i_write,
    input  wire [NB_INST - 1:0]            i_instruction,
    input  wire [NB_ADDR - 1:0]            i_address,
    output wire [NB_INST- 1:0]             o_instruction
);
    
    //Las direcciones las manejamos de 32 bits, para que todo siga funcionando igual. 
    //Pero la memoria la hacemos de 1MB solamente. Porque no tiene sentido tener una de 4GB. 
    reg [NB_INST-1:0] memory[2**NB_ROM_SIZE-1:0];

    
    //Write operation
    always @(negedge i_clk) begin : write                                    // Realizo la escritura durante el semiciclo positivo
        if (i_write)                                                         // del clock y la escritura se encuentre habilitada
            memory[i_address] <= i_instruction;                                        
    end
	
  	//OUTPUT
      //read operation 
  	assign o_instruction = (i_write) ? 32'b0 : memory[i_address];           // El dato se lee al instante y se asigna a la salida
  	                                                                        // De igual modo cuando se escribe se disponibiliza

endmodule
