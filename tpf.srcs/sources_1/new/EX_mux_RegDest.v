`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/05/2021 07:45:17 PM
// Design Name: 
// Module Name: EX_mux_RegDst
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module EX_mux_RegDst
#(
    parameter               NB_TREG         =   5,          // Longitud del campo Shamt
    parameter               NB_SEL          =   2           // Longitud del selector
)
(
    // INPUTS   
    input wire  [NB_TREG-1:0]               i_a,
    input wire  [NB_TREG-1:0]               i_b,
    input wire  [NB_SEL -1:0]               i_sel,
    
    // OUTPUTS
    output wire [NB_TREG-1:0]               o_mux
);
    
    // LOCALPARAMETERS
    localparam                      rt  = 2'b00;                                
    localparam                      rd  = 2'b01;
    localparam                      pc  = 2'b10;
    localparam                      r31 = 5'b11111;
    localparam                      def = 5'b00000;

    // INTERNAL
    reg         [NB_TREG-1:0]               mux;

    always @(*) begin : proc_mux
        case (i_sel)
            
            rt : mux <= i_a;        // RegDest -> RT    
            rd : mux <= i_b;        // RegDest -> RD
            pc : mux <= r31;        // RegDest -> 31 (JAL)      
            default : mux <= def;   // RegDest -> 0
        endcase
    end

    // OUTPUT
    assign o_mux = mux;
    
endmodule
