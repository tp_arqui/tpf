`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/07/2021 07:45:17 PM
// Design Name: 
// Module Name: IF_mux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module IF_mux
#(
    parameter               NB_DATA      =   32                       
)
(
    // INPUTS   
    input wire                          i_select,  
    input wire [NB_DATA-1:0]         i_signal_1,
    input wire [NB_DATA-1:0]         i_signal_2,
    
    // OUTPUTS
	output wire [NB_DATA-1:0]        o_mux
);

    // LOCAL_PARAMETERS
    localparam  SIGNAL_1 = 2'b00;         
    localparam  SIGNAL_2 = 2'b01;        
    
    // INTERNAL
    reg         [NB_DATA-1:0]       mux = 0; 
    
    always @(*) begin
        case (i_select)
            SIGNAL_1: mux <= i_signal_1;
            SIGNAL_2: mux <= i_signal_2;
            default: mux <= 0;
        endcase
    end
    
    // OUTPUT
    assign o_mux = mux;
    
endmodule
