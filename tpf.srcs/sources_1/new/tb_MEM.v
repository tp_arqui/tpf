`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/21/2021 08:53:59 PM
// Design Name: 
// Module Name: tb_MEM_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_MEM();
    
    localparam				   NB_2			= 	2;        
    localparam				   NB_DATA		= 	32;      //Tamanio del dato
    localparam				   NB_ADDR		= 	32;      //Tamanio de la direccion, puede direccionar hasta 4GB
    localparam				   RAM_SIZE		= 	10;      //Tamanio de la memoria, 1 MB
    localparam                 DATA_FILE    =   "arit_logic.mem";      //Nombre del archivo para cargar en la memoria de datos.
    
     // TB_SIGNALS_inputs
    reg                         i_clk;
    
    // MEM_inputs
     reg                              i_clk;     
     reg                              i_MemWrite;
     reg                              i_MemRead; 
     reg                              i_Signed;  
     reg [NB_ADDR-1:0]                i_ALUResult;    
     reg [NB_2-1:0]                   i_Long;    
     reg [NB_DATA-1:0]                i_RegB;
                                                        
     // OUTPUTS                                         
     wire [NB_DATA-1:0]               o_DataRead;
     
     //Primero guardamos 5 datos en las primeras 5 direcciones  
     //Despues leemos los datos y comprobamos que sean correctos
     initial begin
         #10
         i_clk = 1'b0;
         
         #10
         i_MemWrite  = 1'b1;                //Escribimos la memoria
         i_MemRead   = 1'b0;
         i_Signed    = 1'b0;                //El signado no importa cuando se escribe
         i_ALUResult = 32'b00000000_00000000_00000000_00000000;        //La direccion donde escribimos
         i_Long      = 2'b11;               //Si es byte, word o double word. En este caso DW para guardar los 32 bits
         i_RegB      = 32'h11111110;         //Lo que guardamos en memoria.
         
         #20 
         i_ALUResult = 32'b00000000_00000000_00000000_00000001;        //La direccion donde escribimos
         i_RegB      = 32'h11111100;         //Lo que guardamos en memoria.
         
         #20 
         i_ALUResult = 32'b00000000_00000000_00000000_00000010;        //La direccion donde escribimos
         i_RegB      = 32'h11111000;         //Lo que guardamos en memoria.
         
         #20 
         i_ALUResult = 32'b00000000_00000000_00000000_00000011;        //La direccion donde escribimos
         i_RegB      = 32'h11110000;         //Lo que guardamos en memoria.
         
         #20 
         i_ALUResult = 32'b00000000_00000000_00000000_00000100;        //La direccion donde escribimos
         i_RegB      = 32'h11100000;         //Lo que guardamos en memoria.
         
         #20
         i_MemWrite  = 1'b0;                
         i_MemRead   = 1'b1;                 //Leemos la memoria
         i_ALUResult = 32'b00000000_00000000_00000000_00000000;        //La direccion de donde  leemos
         
         #10
         if (o_DataRead != 32'h11111110) begin
                $display("############# Test FALLO ############");
                $finish();
         end
         
         #10
         i_ALUResult = 32'b00000000_00000000_00000000_00000001;        //La direccion de donde  leemos
         
         #10
         if (o_DataRead != 32'h11111100) begin
                $display("############# Test FALLO ############");
                $finish();
         end
         
         #10
         i_ALUResult = 32'b00000000_00000000_00000000_00000010;        //La direccion de donde  leemos
         
         #10
         if (o_DataRead != 32'h11111000) begin
                $display("############# Test FALLO ############");
                $finish();
         end
         
         #10
         i_ALUResult = 32'b00000000_00000000_00000000_00000011;        //La direccion de donde  leemos
         
         #10
         if (o_DataRead != 32'h11110000) begin
                $display("############# Test FALLO ############");
                $finish();
         end
         
         #10
         i_ALUResult = 32'b00000000_00000000_00000000_00000100;        //La direccion de donde  leemos
         
         #10
         if (o_DataRead != 32'h11100000) begin
                $display("############# Test FALLO ############");
                $finish();
         end
         
         
         //Ahora modificamos el byte bajo de la primer direccion y los dos bytes bajos de la segunda direccion y comprobamos que se hayan guardado correctamente
         #20
         i_MemWrite  = 1'b1;                 //Escribimos la memoria                
         i_MemRead   = 1'b0;                 
         i_ALUResult = 32'b00000000_00000000_00000000_00000000;        //La direccion donde  escribimos
         i_Long      = 2'b00;                //Indicamos que escribimos el byte mas bajo
         i_RegB      = 32'h01010101;         //Lo que guardamos en memoria. Solo se consideran los ultimos dos numeros, por ser el byte mas bajo.
         
         #20
         i_Long      = 2'b01;                //Indicamos que escribimos los dos byte mas bajo
         i_ALUResult = 32'b00000000_00000000_00000000_00000001;        //La direccion donde  escribimos
         i_RegB      = 32'h01010101;         //Lo que guardamos en memoria. Solo se consideran los ultimos dos numeros, por ser el byte mas bajo.
         
         //Ahora leemos para verificar que se cambio correctamente los bytes mas bajos,
         //Ademas, probamos la extension de signo
         #20
         i_MemWrite  = 1'b0;                                 
         i_MemRead   = 1'b1;                //Leemos la memoria                 
         i_ALUResult = 32'h00000000;        //La direccion donde  escribimos
         i_Long      = 2'b00;                //Indicamos que leemos el byte mas bajo
         
         //Leemos el byte mas bajo que es 01 y al no tener el signed activado, se rellena con ceros los demas bytes 
         #20
         if (o_DataRead != 32'h00000001) begin
                $display("############# Test FALLO ############");
                $finish();
         end
         
         #10
         i_Signed    = 1'b1;                //Leemos signado, asi que el 0 deberia extenderse
         
         //Leemos el byte mas bajo que es 01 y al tener el signed activado, se rellena con ceros los demas bytes
         #10
         if (o_DataRead != 32'h00000001) begin
                $display("############# Test FALLO ############");
                $finish();
         end
         
         //Ahora leemos la direccion siguiente a la 0
         #10
         i_ALUResult = 32'b00000000_00000000_00000000_00000001;        //La direccion donde  escribimos
         i_Long      = 2'b01;                //Indicamos que leemos los dos byte mas bajo
         i_Signed    = 1'b0;                //Leemos sin signo, asi que se rellena con 0 los dos bytes mas altos
         
         //Leemos los dos byte mas bajo que son 01 y al tener el signed activado, se rellena con ceros los demas bytes
         //En este caso la memoria sigue teniendo 11110101, pero vemos 00000101 porque al leer los bytes mas bajo sin signo, los mas altos se rellenan con cero
         #10
         if (o_DataRead != 32'h00000101) begin
                $display("############# Test FALLO ############");
                $finish();
         end
         
         #10                           
         i_Signed    = 1'b0;               //Leemos signado, asi que el 0 deberia extenderse
         
         
         //Leemos el byte mas bajo que es 01 y al tener el signed activado, se rellena con ceros los demas bytes
         //En este caso la memoria sigue teniendo 11110101, pero vemos 00000101 porque al leer los bytes mas bajo con signo, se extiende el signo, en este caso el 0
         #10
         if (o_DataRead != 32'h00000101) begin
                $display("############# Test FALLO ############");
                $finish();
         end
     end
     
     // CLOCK_GENERATION
     always #10 i_clk = ~i_clk;
       
     MEM
     #(
        .NB_2                   (NB_2),    
        .NB_DATA                (NB_DATA),
        .NB_ADDR                (NB_ADDR),
        .RAM_SIZE               (RAM_SIZE),
        .DATA_FILE              (DATA_FILE)
     )
     u_MEM
     (
        .i_clk                  (i_clk),
        .i_MemWrite             (i_MemWrite),
        .i_MemRead              (i_MemRead),
        .i_Signed               (i_Signed),
        .i_Addr                 (i_ALUResult),
        .i_Long                 (i_Long),
        .i_WriteData            (i_RegB),
        .o_ReadData             (o_DataRead)
     );
endmodule
