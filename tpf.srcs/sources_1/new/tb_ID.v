`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/13/2021 09:31:48 PM
// Design Name: 
// Module Name: tb_ID
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_ID();

	localparam NB_INST   		= 32;
    localparam NB_ADDR        	= 26;
    localparam NB_INMED       	= 16;        
    localparam NB_OPCODE      	= 6;
    localparam NB_TREG      	= 5;
    localparam NB_ALUOP         = 3;
    localparam NB_2          	= 2;

    // INSTRUCTION
    localparam ADDU          	= 32'b000000_00001_00010_00110_00000_100001;
    localparam XOR          	= 32'b000000_00010_00011_00110_00000_100100;
    localparam SLL          	= 32'b000000_00001_00100_00110_00011_000000;
    localparam LB	          	= 32'b100000_00000_00000_0000000000001000;
    localparam LH	        	= 32'b100001_00001_00000_0000000000011000;
    localparam LW	          	= 32'b100011_00010_00000_0000000000111000;
    localparam LBU	          	= 32'b100100_00000_00000_0000000000001000;
    localparam LHU	        	= 32'b100101_00001_00000_0000000000011000;
    localparam LWU	          	= 32'b100111_00010_00000_0000000000111000;
    localparam SB	          	= 32'b101000_00000_00000_0000000000001000;
    localparam SH	        	= 32'b101001_00001_00000_0000000000011000;
    localparam SW	          	= 32'b101011_00010_00000_0000000000111000;
    localparam ADDI	          	= 32'b001000_00011_00100_1000000000001111;
    localparam LUI	        	= 32'b001111_00000_00000_0000000000000011;
    localparam BEQ	          	= 32'b000100_00100_00100_0000000011111111;
    localparam BNE	          	= 32'b000101_00011_00100_0000111111111111;
	localparam J	          	= 32'b000010_00000000000000000000000001;
	localparam JAL	          	= 32'b000011_00000000000000000000000010;
	localparam JR	          	= 32'b000000_00011_00000_00000_00000_001000;
	localparam JALR	          	= 32'b000000_00010_00000_00110_00000_001001;
    localparam HALT             = 32'b0;

    
    // TB_SIGNALS_inputs
    reg                         i_clk;
    reg                         i_reset;
    // ID_inputs
    reg [NB_INST-1:0]          	i_instruction;
    reg [NB_INST-1:0]			i_pc;
    reg [NB_INST-1:0]			i_wreg_addr;
    reg [NB_INST-1:0]			i_wreg_data;

    // OUTPUTS
    // FETCH
    wire [NB_INST-1:0]			o_jump_addr;
    wire [NB_INST-1:0]			o_branch_addr;
    wire [NB_INST-1:0]			o_jr_addr;
    wire						o_Branch;
    wire						o_Jump;
    wire						o_Jr;
    // EX
    wire [NB_INST-1:0]			o_reg_a;		
    wire [NB_INST-1:0]			o_reg_b;			
    wire [NB_INST-1:0]			o_signext;
    wire [NB_2-1:0]				o_RegDst;
    wire [NB_ALUOP-1:0]			o_ALUOp;
    wire						o_ALUSrc;						
    wire [NB_TREG-1:0]			o_reg_rt;
    wire [NB_TREG-1:0]			o_reg_rd;
    wire [NB_TREG-1:0]			o_shamt;
    // MEM
    wire						o_MemRead;
    wire						o_MemWrite;
    wire						o_Signed;
    wire [NB_2-1:0]	    		o_Long;
    // WB
    wire [NB_2-1:0]				o_MemtoReg;	 
    wire                        o_RegWrite;					
    wire						o_Halt;


    initial begin
        #10
        i_clk = 1'b0;
        i_reset = 1'b1;
        i_pc = 32'b100;
        i_wreg_addr = 5'b1000;
        i_wreg_data = 32'b11111111;
                
        #10
        i_reset = 1'b0;

        // R-TYPE
        i_instruction = ADDU;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd1) || (o_reg_b != 32'd2) || (o_RegWrite != 1'b1) ||
        	(o_RegDst != 2'b01) || (o_ALUOp != 3'b011) || (o_ALUSrc != 1'b0) || (o_MemRead != 1'b0) || (o_MemWrite != 1'b0) ||
        	(o_MemtoReg != 2'b00) || (o_Halt != 1'b0) || (o_signext[5:0] != 6'b100001) || (o_reg_rd != 5'h6)) begin
                $display("############# Test FALLO ############");
                $finish();
    	end

    	i_instruction = XOR;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd2) || (o_reg_b != 32'd3) || (o_RegWrite != 1'b1) ||
        	(o_RegDst != 2'b01) || (o_ALUOp != 3'b011) || (o_ALUSrc != 1'b0) || (o_MemRead != 1'b0) || (o_MemWrite != 1'b0) ||
        	(o_MemtoReg != 2'b00) || (o_Halt != 1'b0) || (o_signext[5:0] != 6'b100100) || (o_reg_rd != 5'h6)) begin
                $display("############# Test FALLO ############");
                $finish();
    	end

    	i_instruction = SLL;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd0) || (o_reg_b != 32'd4) || (o_RegWrite != 1'b1) ||
        	(o_RegDst != 2'b01) || (o_ALUOp != 3'b011) || (o_ALUSrc != 1'b0) || (o_MemRead != 1'b0) || (o_MemWrite != 1'b0) ||
        	(o_MemtoReg != 2'b00) || (o_Halt != 1'b0) || (o_signext[5:0] != 6'b000000) || (o_shamt != 5'd3) || (o_reg_rd != 5'h6)) begin
                $display("############# Test FALLO ############");
                $finish();
    	end
        
        // LOAD
        i_instruction = LB;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd0) || (o_reg_b != 32'd0) || (o_RegWrite != 1'b1) ||
        	(o_RegDst != 2'b00) || (o_ALUOp != 3'b000) || (o_ALUSrc != 1'b1) || (o_MemRead != 1'b1) || (o_MemWrite != 1'b0) ||
        	(o_MemtoReg != 2'b01) || (o_Halt != 1'b0) || (o_signext != $signed(16'd8)) || (o_Signed != 1'b1) || (o_Long != 2'b00)) begin
                $display("############# Test FALLO ############");
                $finish();
    	end
    	
    	i_instruction = LH;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd1) || (o_reg_b != 32'd0) || (o_RegWrite != 1'b1) ||
        	(o_RegDst != 2'b00) || (o_ALUOp != 3'b000) || (o_ALUSrc != 1'b1) || (o_MemRead != 1'b1) || (o_MemWrite != 1'b0) ||
        	(o_MemtoReg != 2'b01) || (o_Halt != 1'b0) || (o_signext != $signed(16'd24)) || (o_Signed != 1'b1) || (o_Long != 2'b01)) begin
                $display("############# Test FALLO ############");
                $finish();
    	end
    	
    	i_instruction = LW;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd2) || (o_reg_b != 32'd0) || (o_RegWrite != 1'b1) ||
        	(o_RegDst != 2'b00) || (o_ALUOp != 3'b000) || (o_ALUSrc != 1'b1) || (o_MemRead != 1'b1) || (o_MemWrite != 1'b0) ||
        	(o_MemtoReg != 2'b01) || (o_Halt != 1'b0) || (o_signext != $signed(16'd56)) || (o_Signed != 1'b1) || (o_Long != 2'b11)) begin
                $display("############# Test FALLO ############");
                $finish();
    	end
    	
    	i_instruction = LBU;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd0) || (o_reg_b != 32'd0) || (o_RegWrite != 1'b1) ||
        	(o_RegDst != 2'b00) || (o_ALUOp != 3'b000) || (o_ALUSrc != 1'b1) || (o_MemRead != 1'b1) || (o_MemWrite != 1'b0) ||
        	(o_MemtoReg != 2'b01) || (o_Halt != 1'b0) || (o_signext != $signed(16'd8)) || (o_Signed != 1'b0) || (o_Long != 2'b00)) begin
                $display("############# Test FALLO ############");
                $finish();
    	end
    	
        i_instruction = LHU;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd1) || (o_reg_b != 32'd0) || (o_RegWrite != 1'b1) ||
        	(o_RegDst != 2'b00) || (o_ALUOp != 3'b000) || (o_ALUSrc != 1'b1) || (o_MemRead != 1'b1) || (o_MemWrite != 1'b0) ||
        	(o_MemtoReg != 2'b01) || (o_Halt != 1'b0) || (o_signext != $signed(16'd24)) || (o_Signed != 1'b0) || (o_Long != 2'b01)) begin
                $display("############# Test FALLO ############");
                $finish();
    	end
    	
    	i_instruction = LWU;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd2) || (o_reg_b != 32'd0) || (o_RegWrite != 1'b1) ||
        	(o_RegDst != 2'b00) || (o_ALUOp != 3'b000) || (o_ALUSrc != 1'b1) || (o_MemRead != 1'b1) || (o_MemWrite != 1'b0) ||
        	(o_MemtoReg != 2'b01) || (o_Halt != 1'b0) || (o_signext != $signed(16'd56)) || (o_Signed != 1'b0) || (o_Long != 2'b11)) begin
                $display("############# Test FALLO ############");
                $finish();
    	end


        // STORE
        i_instruction = SB;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd0) || (o_reg_b != 32'd0) || (o_RegWrite != 1'b0) ||
            (o_ALUOp != 3'b000) || (o_ALUSrc != 1'b1) || (o_MemRead != 1'b0) || (o_MemWrite != 1'b1) || (o_Halt != 1'b0) || 
            (o_signext != $signed(16'd8)) || (o_Long != 2'b00)) begin
                $display("############# Test FALLO ############");
                $finish();
        end
        
        i_instruction = SH;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd1) || (o_reg_b != 32'd0) || (o_RegWrite != 1'b0) ||
            (o_ALUOp != 3'b000) || (o_ALUSrc != 1'b1) || (o_MemRead != 1'b0) || (o_MemWrite != 1'b1) || (o_Halt != 1'b0) || 
            (o_signext != $signed(16'd24)) || (o_Long != 2'b01)) begin
                $display("############# Test FALLO ############");
                $finish();
        end
        
        i_instruction = SW;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd2) || (o_reg_b != 32'd0) || (o_RegWrite != 1'b0) ||
            (o_ALUOp != 3'b000) || (o_ALUSrc != 1'b1) || (o_MemRead != 1'b0) || (o_MemWrite != 1'b1) || (o_Halt != 1'b0) ||
            (o_signext != $signed(16'd56)) || (o_Long != 2'b11)) begin
                $display("############# Test FALLO ############");
                $finish();
        end

        // INMEDIATE
        i_instruction = ADDI;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd3) || (o_reg_b != 32'd4) || (o_RegWrite != 1'b1) ||
            (o_RegDst != 2'b00) ||  (o_ALUOp != 3'b000) || (o_ALUSrc != 1'b1) || (o_MemRead != 1'b0) || (o_MemWrite != 1'b0) ||
            (o_MemtoReg != 2'b00) || (o_Halt != 1'b0) || (o_signext != 32'hFFFF800F)) begin
                $display("############# Test FALLO ############");
                $finish();
        end
        
        i_instruction = LUI;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd0) || (o_reg_b != 32'd0) || (o_RegWrite != 1'b1) ||
            (o_RegDst != 2'b00) || (o_ALUOp != 3'b111) || (o_ALUSrc != 1'b1) || (o_MemRead != 1'b0) || (o_MemWrite != 1'b0) ||
            (o_MemtoReg != 2'b00) || (o_Halt != 1'b0) || (o_signext != $signed(16'd3))) begin
                $display("############# Test FALLO ############");
                $finish();
        end

        // BRANCH
        i_instruction = BEQ;
        #20
        if ((o_Branch != 1'b1) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd4) || (o_reg_b != 32'd4) || 
            (o_ALUOp != 3'b001) || (o_ALUSrc != 1'b0) || (o_MemRead != 1'b0) || (o_MemWrite != 1'b0) || (o_RegWrite != 1'b0) ||
            (o_Halt != 1'b0) || (o_signext != $signed(16'hFF)) || (o_branch_addr != (i_pc+$signed(16'hFF)))) begin
                $display("############# Test FALLO ############");
                $finish();
        end
        
        i_instruction = BNE;

        #20
        if ((o_Branch != 1'b1) || (o_Jump != 1'b0) || (o_Jr != 1'b0) || (o_reg_a != 32'd3) || (o_reg_b != 32'd4) ||
            (o_ALUOp != 3'b001) || (o_ALUSrc != 1'b0) || (o_MemRead != 1'b0) || (o_MemWrite != 1'b0) || (o_RegWrite != 1'b0) ||
            (o_Halt != 1'b0) || (o_signext != $signed(16'hFFF)) || (o_branch_addr != (i_pc+$signed(16'hFFF)))) begin
                $display("############# Test FALLO ############");
                $finish();
        end

        // JUMP
        i_instruction = J;
        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b1) || (o_Jr != 1'b0) || (o_MemWrite != 1'b0) || (o_Halt != 1'b0) ||
            (o_signext != $signed(16'd1)) || (o_jump_addr != {i_pc[31:26],26'b1}) || (o_RegWrite != 1'b0)) begin
                $display("############# Test FALLO ############");
                $finish();
        end
        
        i_instruction = JAL;

        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b1) || (o_Jr != 1'b0) || (o_MemWrite != 1'b0) || (o_Halt != 1'b0) ||
            (o_RegDst != 2'b10) || (o_MemtoReg != 2'b10) || (o_signext != $signed(16'd2))  || (o_RegWrite != 1'b1) ||
            (o_jump_addr != {i_pc[31:26],26'b10})) begin
                $display("############# Test FALLO ############");
                $finish();
        end

        // J-TYPE
        i_instruction = JR;
        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b1) || (o_reg_a != 32'd3) || (o_reg_b != 32'd0) || (o_RegWrite != 1'b1) ||
            (o_RegDst != 2'b01) || (o_ALUOp != 3'b011) || (o_ALUSrc != 1'b0) || (o_MemRead != 1'b0) || (o_MemWrite != 1'b0) ||
            (o_MemtoReg != 2'b00) || (o_Halt != 1'b0) || (o_signext[5:0] != 6'b001000) || (o_jr_addr != 32'd8)) begin
                $display("############# Test FALLO ############");
                $finish();
        end
        
        i_instruction = JALR;
        #20
        if ((o_Branch != 1'b0) || (o_Jump != 1'b0) || (o_Jr != 1'b1) || (o_reg_a != 32'd2) || (o_reg_b != 32'd0) || (o_RegWrite != 1'b1) ||
            (o_RegDst != 2'b01) || (o_ALUOp != 3'b011) || (o_ALUSrc != 1'b0) || (o_MemRead != 1'b0) || (o_MemWrite != 1'b0) ||
            (o_MemtoReg != 2'b00) || (o_Halt != 1'b0) || (o_signext[5:0] != 6'b001001) || (o_reg_rd != 5'h6)  ||
            (o_jr_addr != 32'd6)) begin
                $display("############# Test FALLO ############");
                $finish();
        end

        #100
        
        $display("############# Test OK ############");
        $finish();
                
    end

    // CLOCK_GENERATION
    always #10 i_clk = ~i_clk;

    ID
    #(
    	.NB_INST     		(NB_INST),
       	.NB_ADDR          	(NB_ADDR),
       	.NB_INMED	        (NB_INMED),
       	.NB_OPCODE        	(NB_OPCODE),
       	.NB_TREG        	(NB_TREG),
       	.NB_ALUOP           (NB_ALUOP),
       	.NB_2           	(NB_2)
	)
	u_ID
	(
		.i_clk				(i_clk),
		.i_reset			(i_reset),
		.i_instruction		(i_instruction),
		.i_pc				(i_pc),
		.i_wreg_addr		(i_wreg_addr),
		.i_wreg_data		(i_wreg_data),
		// FETCH
		.o_jump_addr		(o_jump_addr),
		.o_branch_addr		(o_branch_addr),
		.o_jr_addr			(o_jr_addr),
		.o_Branch 			(o_Branch),
		.o_Jump 			(o_Jump),
		.o_Jr 				(o_Jr),
		// EX
		.o_reg_a 			(o_reg_a),
		.o_reg_b			(o_reg_b),
		.o_signext			(o_signext),
		.o_RegDst			(o_RegDst),
		.o_ALUOp			(o_ALUOp),
		.o_ALUSrc			(o_ALUSrc),
		.o_reg_rt			(o_reg_rt),
		.o_reg_rd			(o_reg_rd),
		.o_shamt			(o_shamt),
		// MEM
		.o_MemRead			(o_MemRead),
		.o_MemWrite			(o_MemWrite),
		.o_Signed			(o_Signed),
		.o_Long				(o_Long),
		// WB
		.o_MemtoReg			(o_MemtoReg),
        .o_RegWrite         (o_RegWrite),
		.o_Halt				(o_Halt)
	);
endmodule
