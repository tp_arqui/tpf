`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/05/2021 07:32:38 PM
// Design Name: 
// Module Name: ID_reg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ID_reg
#(
	parameter               NB_REG         =   32,         // Longitud de registro con signo
	parameter				NB_TREG		   =   5		   // Longitud del campo RS,RT,RD
)
(
    // INPUTS   
    input wire					           	i_clk,
    input wire					           	i_reset,
    input wire					           	i_RegWrite,
    input wire  [NB_TREG-1:0]           	i_reg_a,
    input wire  [NB_TREG-1:0]           	i_reg_b,
    input wire  [NB_TREG-1:0]           	i_wreg_addr,
    input wire  [NB_REG-1:0]	          	i_wreg_data,
    
    // OUTPUTS
	output wire [NB_REG-1:0]   	    		o_reg_a,
	output wire [NB_REG-1:0]       			o_reg_b
);

	// INTERNAL
	reg         [NB_REG-1:0]        		registers[0:NB_REG-1];		// Array de registros de 32 bits de largo y 32 de ancho
	reg 		[NB_REG-1:0]				reg_aux_a;
	reg 		[NB_REG-1:0]				reg_aux_b;
	integer i;

	initial begin
        for (i = 0 ; i < NB_REG ; i = i+1)  
            registers[i] = i;
    end

	always @(negedge i_clk) begin: proc_read							// Leo los registros en el flanco descendente para evitar
		reg_aux_a <= registers[i_reg_a];								// los riesgos estructurales en el banco de registros
		reg_aux_b <= registers[i_reg_b];
	end

	always @(posedge i_clk) begin : proc_write							// Escribo los registros en el flanco ascendente para evitar
		if(i_RegWrite && (i_wreg_addr != 5'b0)) begin					// los riesgos estructurales en el banco de registros
			registers[i_wreg_addr] <= i_wreg_data;						// i=5 para tb_ID (no hay polque)
		end 															// Cableo R0 a 0, ya que no se usa
	end

	// OUTPUT
	assign o_reg_a = reg_aux_a;
	assign o_reg_b = reg_aux_b;

endmodule
