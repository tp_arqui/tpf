`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/15/2021 07:45:17 PM
// Design Name: 
// Module Name: EX_ALUCtrl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module EX_ALUCtrl
#(
    parameter				NB_OPCODE		=	6,			// Longitud del OPCODE
    parameter				NB_ALUOP		=	3,			// Longitud del ALUOP    
    parameter               NB_OPER         =   4			// Longitud del operando de ALU
)
(
	// INPUTS   
    input wire  [NB_OPCODE-1:0]				i_funct,
    input wire  [NB_ALUOP-1:0] 				i_ALUOp,
    
    // OUTPUTS
    output wire [NB_OPER-1:0]               o_Oper
);

	// LOCALPARAMETERS
	// INPUTS
	localparam F_SLL   = 9'b011_000000;
	localparam F_SRL   = 9'b011_000010;
	localparam F_SRA   = 9'b011_000011;
	localparam F_SLLV  = 9'b011_000100;
	localparam F_SRLV  = 9'b011_000110;
	localparam F_SRAV  = 9'b011_000111;
	localparam F_ADDU  = 9'b011_100001;
	localparam F_SUBU  = 9'b011_100011;
	localparam F_AND   = 9'b011_100100;
	localparam F_OR    = 9'b011_100101;
	localparam F_XOR   = 9'b011_100110;
	localparam F_NOR   = 9'b011_100111;
	localparam F_SLT   = 9'b011_101010;
	localparam F_LOADS = 9'b000_??????;
	localparam F_ANDI  = 9'b100_??????;
	localparam F_ORI   = 9'b101_??????;
	localparam F_XORI  = 9'b110_??????;
	localparam F_LUI   = 9'b111_??????;
	localparam F_SLTI  = 9'b010_??????;
	localparam F_BEQ   = 9'b001_??????;
	localparam F_JALR  = 9'b011_001001;

  	// OUTPUTS
	localparam OUT_SLL  = 4'b0000;
	localparam OUT_SRL  = 4'b0001;
	localparam OUT_SRA  = 4'b0010;
	localparam OUT_SLLV = 4'b0011;
	localparam OUT_SRLV = 4'b0100;
	localparam OUT_SRAV = 4'b0101;
	localparam OUT_ADDU = 4'b0110;
	localparam OUT_SUBU = 4'b0111;
	localparam OUT_AND  = 4'b1000;
	localparam OUT_OR   = 4'b1001;
	localparam OUT_XOR  = 4'b1010;
	localparam OUT_NOR  = 4'b1011;
	localparam OUT_SLT  = 4'b1100;
	localparam OUT_JALR = 4'b1101;
	localparam OUT_LUI	= 4'b1110;

	// INTERNAL
	reg [NB_OPER-1:0]               		Oper;

	always @(*) begin : proc_Oper

		casez ({i_ALUOp,i_funct})

			F_SLL  : Oper = OUT_SLL ;
			F_SRL  : Oper = OUT_SRL ;
			F_SRA  : Oper = OUT_SRA ;
			F_SLLV : Oper = OUT_SLLV;
			F_SRLV : Oper = OUT_SRLV;
			F_SRAV : Oper = OUT_SRAV;
			F_ADDU : Oper = OUT_ADDU;
			F_SUBU : Oper = OUT_SUBU;
			F_AND  : Oper = OUT_AND ;
			F_OR   : Oper = OUT_OR  ;
			F_XOR  : Oper = OUT_XOR ;
			F_NOR  : Oper = OUT_NOR ;
			F_SLT  : Oper = OUT_SLT ;

			F_LOADS : Oper = OUT_ADDU; // Incluye ADDI

			F_ANDI : Oper = OUT_AND;
			F_ORI  : Oper = OUT_OR;
			F_XORI : Oper = OUT_XOR;
			F_LUI  : Oper = OUT_LUI;
			F_SLTI : Oper = OUT_SLT;

			F_BEQ  : Oper = OUT_SUBU;

			F_JALR : Oper = OUT_JALR;
			
			default : Oper = 4'b1111;
		endcase
	end

	// OUTPUT
	assign o_Oper = Oper;

endmodule