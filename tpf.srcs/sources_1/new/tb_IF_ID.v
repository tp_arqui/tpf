`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/15/2021 11:15:09 AM
// Design Name: 
// Module Name: tb_IF
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_IF_ID();

    localparam NB_INST   		= 32;        
    localparam NB_ADDR        	= 32;        //Memoria de instrucciones de 4GB
    
    // TB_SIGNALS_inputs
    reg                         i_clk;
    reg                         i_reset;    
    reg                         i_stall;    
    reg                         i_flush;    
    
    
    // IF_inputs
    reg                         i_write;        //Indica si se escribe la memoria de instrucciones   
    reg                         i_enable;        //Para detener el PC y por lo tanto detener la etapa.    
    reg                         i_Branch;        //Para salto condicional, sumando la parte baja de la instruccion al PC. 
    reg                         i_Jump;          //Para salto incondicional, leyendo direccion desde parte baja de la instruccion.  
    reg                         i_Jr;            //Para salto incondicional, leyendo nueva direccion desde registro. 
    reg [NB_ADDR-1:0]           i_jr_addr;       //Para salto JR. Necesito la direccion desde el registro 31
    reg [NB_ADDR-1:0]           i_jump_addr;     //Para salto incondicional. Necesito nueva direccion a donde saltar.
    reg [NB_ADDR-1:0]           i_branch_addr;   //Para salto condicional. Necesito nueva direccion a donde saltar.
    reg [NB_INST-1:0]           i_instruction;   //Instruccion para escribir en memoria
    
    //OUTPUTS
    wire [NB_INST-1 : 0]        o_instruction;      //Instruccion leida de la memoria.
    wire [NB_INST-1 : 0]        o_instruction_latch;
    wire [NB_ADDR-1 : 0]        o_pc_sum;            //Proxima direccion a leer de memoria de inst. PC + 1
    wire [NB_ADDR-1 : 0]        o_pc_sum_latch;      
   
   
   initial begin
        #10
        i_clk = 1'b0;
        i_reset = 1'b1;
        
        #10
        i_reset = 1'b0;
        //Seniales IF_ID
        i_stall = 1'b0;
        i_flush = 1'b0;
        
        //Seniales IF
        //Instancio IF para no tener que generar las seniales a mano en el TB.
        i_enable = 1'b1;
        i_write = 1'b1;
        i_Branch = 1'b0;
        i_Jump = 1'b0;
        i_Jr = 1'b0;
        i_jr_addr = 10'b1111111111;        //Para identificar si en algun momento salta a esta direccion. Solo deberia saltar cuando se hace el test.
        i_jump_addr = 10'b1111111111;      //Son de 10 bits porque la memoria es de 1MB, osea, tiene 2^10 direcciones 
        i_branch_addr = 10'b1111111111;
        i_instruction = 32'h0000001;        //La instruccion que vamos a escribir en la memoria    
        
        //Escribo en memoria 5 instrucciones
        #20
        i_instruction = 32'h0000011;    //La instruccion que vamos a escribir en la memoria
        
        #20
        i_instruction = 32'h0000111;    //La instruccion que vamos a escribir en la memoria
        
        #20
        i_instruction = 32'h0001111;    //La instruccion que vamos a escribir en la memoria
        
        #20
        i_instruction = 32'h0011111;    //La instruccion que vamos a escribir en la memoria
        
        #10
         i_reset = 1'b1;                 //Reseteo para empezar a leer el programa que acabamos de escribir
         i_write = 1'b0;                 //Dejo de escribir la memoria
        
        #20
        i_reset = 1'b0;
        
        #20                             //pruebo el stall justo despues de reset. Entonces, deberia tener la instruccion de la primera direccion, porque viene un ciclo atrasado
        i_stall = 1'b1;
        
        #20                             //Despues de dos ciclos, si no estuviera el stall la instruccion deberia ser 32'h00000111, pero como pusimos el stall tiene que seguir siendo 32'h00000001  
        i_stall = 1'b0;
        if (o_instruction_latch != 32'h00000001) begin
                $display("############# Test FALLO ############");
                $finish();
    	end
    	
    	#20 
    	i_flush = 1'b1;
    	
    	#20
    	i_flush = 1'b0;
    	if (o_instruction_latch != 32'h00000000) begin
                $display("############# Test FALLO ############");
                $finish();
    	end
    	
        /*#20
        i_Branch = 1'b1;
        i_branch_addr = 10'b0000000011; //Saltamos a la direccion 3 que deberia tener como instruccion 32'h0001111
        
         #20
        i_Branch = 1'b0;                //Seguimos el programa a partir del salto y controlamos que siga en la posicion 4 que deberia tener 32'h0011111
       
        #10
        if (o_instruction != 32'h0001111) begin
                $display("############# Test FALLO ############");
                $finish();
    	end
        
        #10
        if (o_instruction != 32'h0011111) begin
                $display("############# Test FALLO ############");
                $finish();
    	end       

        #20
        i_Jump = 1'b1;
        i_jump_addr = 10'b0000000011; //Saltamos a la direccion 3 que deberia tener como instruccion 32'h0001111
        if (o_instruction != 32'h0001111) begin
                $display("############# Test FALLO ############");
                $finish();
    	end
        
        #20
        i_Jump = 1'b0;                //Seguimos el programa a partir del salto y controlamos que siga en la posicion 4 que deberia tener 32'h0011111
        if (o_instruction != 32'h0011111) begin
                $display("############# Test FALLO ############");
                $finish();
    	end  
    	
        #20
        i_Jr = 1'b1;
        i_jr_addr = 10'b0000000011; //Saltamos a la direccion 3 que deberia tener como instruccion 32'h0001111
        if (o_instruction != 32'h0001111) begin
                $display("############# Test FALLO ############");
                $finish();
    	end
        
        #20
        i_Jr = 1'b0;                //Seguimos el programa a partir del salto y controlamos que siga en la posicion 4 que deberia tener 32'h0011111
        if (o_instruction != 32'h0011111) begin
                $display("############# Test FALLO ############");
                $finish();
    	end     
    	
    	#20                        //Pruebo que cuando deshabilito el PC, deje de contar. 
    	i_enable = 1'b0;
        if (o_pc_sum != 10'b0000000100) begin                   //Como salte a la direccion 3, tengo que ver que el PC_sum, siga siendo 4, que es la proxima direccion
                $display("############# Test FALLO ############");
                $finish();
        end*/
   end
   
   // CLOCK_GENERATION
   always #10 i_clk = ~i_clk;
   
   IF
   #(
        .NB_ADDR                (NB_ADDR),
        .NB_INST                (NB_INST)   
   )
   u_IF
   (
        .i_clk                  (i_clk),
        .i_reset                (i_reset),
        .i_write                (i_write),
        .i_enable               (i_enable),
        .i_Branch               (i_Branch),
        .i_Jump                 (i_Jump),
        .i_Jr                   (i_Jr),
        .i_jr_addr              (i_jr_addr),
        .i_jump_addr            (i_jump_addr),
        .i_branch_addr          (i_branch_addr),
        .i_instruction          (i_instruction),
        .o_instruction          (o_instruction),
        .o_pc_sum               (o_pc_sum)
   );
    
   IF_ID
   #(
        .NB_ADDR                (NB_ADDR),
        .NB_INST                (NB_INST)
   ) 
   u_IF_ID
   (
        .i_clk                  (i_clk),
        .i_reset                (i_reset),
        .i_stall                (i_stall),
        .i_flush                (i_flush),
        .i_instruction          (o_instruction),
        .i_pc_sum               (o_pc_sum),
        .o_instruction          (o_instruction_latch),
        .o_pc_sum               (o_pc_sum_latch)
   );
endmodule
