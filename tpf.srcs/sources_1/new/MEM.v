`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/16/2021 11:31:39 AM
// Design Name: 
// Module Name: MEM_data_memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MEM
#(  
    parameter				   NB_2			= 	2,      
    parameter				   NB_DATA		= 	32,     //Tamanio del dato
    parameter				   NB_ADDR		= 	32,     //Tamanio de la direccion, puede direccionar hasta 4GB
    parameter				   RAM_SIZE		= 	10,     //Tamanio de la memoria, 1 MB
    parameter                  DATA_FILE    =   ""      //Nombre del archivo para cargar en la memoria de datos.
)
(
     // INPUTS
    input wire                              i_clk,          
    input wire                              i_MemWrite,     //Senial para escribir la memoria
    input wire                              i_MemRead,      //Senial para leer la memoria
    input wire                              i_Signed,       //Senial que indica si 
    input wire [NB_ADDR-1:0]                i_Addr,
    input wire [NB_2-1:0]                   i_Long,
    input wire [NB_DATA-1:0]                i_WriteData,
    
    // OUTPUTS
	output wire [NB_DATA-1:0]               o_ReadData    
);

    MEM_data_memory
    #(
        .NB_2                   (NB_2),    
        .NB_DATA                (NB_DATA),
        .NB_ADDR                (NB_ADDR),
        .RAM_SIZE               (RAM_SIZE),
        .DATA_FILE               (DATA_FILE)
    )
    u_MEM_data_memory
    (
        .i_clk                  (i_clk),
        .i_MemWrite             (i_MemWrite),
        .i_MemRead              (i_MemRead),
        .i_Signed               (i_Signed),
        .i_Addr                 (i_Addr),
        .i_Long                 (i_Long),
        .i_WriteData            (i_WriteData),
        .o_ReadData             (o_ReadData)
    );
endmodule