`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/07/2021 07:45:17 PM
// Design Name: 
// Module Name: IF_mux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module IF_pc
#(
	parameter NB_ADDR = 32	//Tamaño de la memoria de instrucciones 4 GB
)
(
	input wire i_clk, 
	input wire i_enable, //Detiene el contador de pograma. Por ej, cuando hay un halt. 
	input wire i_reset,
	input wire [NB_ADDR - 1:0] i_pc, // d
	output wire [NB_ADDR - 1:0] o_pc // q
);

    //INTERNAL
    reg [NB_ADDR - 1:0] pc_reg=0, pc_next=0; // la direccion inicial en 0

    always@(posedge i_clk)
    begin : next_address
        if (i_reset)
           pc_reg<=0;
        else if (i_enable)   
            pc_reg<=pc_next;
    end
    
    always @(*)
        pc_next = i_pc;              //Cuando cambia pc_in, en el proximo clock cambio el pc_out. Y cambio la direccion
        

    //OUTPUT
    assign o_pc = pc_reg;	

endmodule
