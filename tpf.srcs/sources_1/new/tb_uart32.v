`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/29/2021 09:05:34 PM
// Design Name: 
// Module Name: tb_uart32
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_uart32();

	localparam NB_INST		= 	32;     	// Tamanio de instruccion
	localparam SB_TICK      =   16;
	localparam DATA     	= 	32'hABCDEF;


    // TB_SIGNALS
    reg                     i_clk;
    reg                     i_reset;
    // UART
    reg     [NB_INST-1:0]   i_tx;
    reg                     i_tx_start;
    wire                    o_tx;
    wire                    o_tx_done;
    wire                    o_rx_done;
    wire    [NB_INST-1:0]   o_rx;

    initial begin
        #10
        i_clk = 1'b0;
        i_reset = 1'b1;
        i_tx_start = 1'b0;
        i_tx = DATA;
                
        #10
        i_reset = 1'b0;

        #20
        i_tx_start = 1'b1;

        #20
        i_tx_start = 1'b0;
        
        #8000000
        
        $display("############# Test OK ############");
        $finish();
                
    end

    // CLOCK_GENERATION
    always #10 i_clk = ~i_clk;

   	uart
    #(
        .NB_DATA          (NB_INST),
        .SB_TICK          (SB_TICK)
    )
    u_uart
    (
        .i_clk            (i_clk),
        .i_reset          (i_reset), 
        .i_rx             (o_tx), 
        .i_tx_start       (i_tx_start),
        .i_tx             (i_tx),
        .o_tx             (o_tx),
        .o_tx_done        (o_tx_done),
        .o_rx_done        (o_rx_done),
        .o_rx             (o_rx)
    );

    always @(posedge i_clk) begin       
        if (o_rx_done) begin            // Verifico datos transmitidos
             if (DATA != o_rx) begin
                 $display("############# Test FALLO ############");
                 $finish();
             end
        end
    end

endmodule
