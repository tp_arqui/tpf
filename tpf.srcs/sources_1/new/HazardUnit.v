`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/24/2021 02:18:56 PM
// Design Name: 
// Module Name: HazardUnit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module HazardUnit
#(
	parameter NB_TREG		=   5	   		// Longitud del campo RS,RT,RD
)
(
	// INPUTS
	input wire 	[NB_TREG-1:0]       i_reg_rs_ID,
    input wire 	[NB_TREG-1:0]       i_reg_rt_ID,
    input wire 	[NB_TREG-1:0]       i_reg_rt_EX,
    input wire 				        i_MemRead_EX,

    // OUTPUTS
    output wire						o_stall
);

	// OUTPUT
	assign o_stall = ((i_MemRead_EX) && 
		((i_reg_rt_EX == i_reg_rs_ID) || (i_reg_rt_EX == i_reg_rt_ID)));

endmodule
