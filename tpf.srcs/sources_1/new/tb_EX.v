`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/19/2021 06:11:36 PM
// Design Name: 
// Module Name: tb_EX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_EX();

	localparam NB_INST   		= 32;
    localparam NB_OPCODE      	= 6;
    localparam NB_TREG      	= 5;
    localparam NB_OPER         	= 4;
    localparam NB_ALUOP         = 3;
    localparam NB_2          	= 2;

	// OPERATIONS
	// R-TYPE
	localparam SLL   = 6'b000000;
	localparam SRL   = 6'b000010;
	localparam SRA   = 6'b000011;
	localparam SLLV  = 6'b000100;
	localparam SRLV  = 6'b000110;
	localparam SRAV  = 6'b000111;
	localparam ADDU  = 6'b100001;
	localparam SUBU  = 6'b100011;
	localparam AND   = 6'b100100;
	localparam OR    = 6'b100101;
	localparam XOR   = 6'b100110;
	localparam NOR   = 6'b100111;
	localparam SLT   = 6'b101010;
	localparam JALR  = 6'b001001;	
	// I-TYPE
	localparam RTYPE = 3'b011;
	localparam LOADS = 3'b000;
	localparam ANDI  = 3'b100;
	localparam ORI   = 3'b101;
	localparam XORI  = 3'b110;
	localparam LUI   = 3'b111;
	localparam SLTI  = 3'b010;
	localparam BEQ   = 6'b001;

    // TB_SIGNALS
    // INPUTS
    reg                         i_clk;
    reg                         i_reset;
    reg [NB_INST-1:0]			i_pc;
    // EX
    reg [NB_ALUOP-1:0]			i_ALUOp;
    reg							i_ALUSrc;						
    reg [NB_2-1:0]				i_RegDst;
    reg [NB_INST-1:0]			i_reg_a;		
    reg [NB_INST-1:0]			i_reg_b;			
    reg [NB_INST-1:0]			i_signext;
    reg [NB_TREG-1:0]			i_shamt;
    reg [NB_TREG-1:0]			i_reg_rt;
    reg [NB_TREG-1:0]			i_reg_rd;
    reg  						i_Jr;
    // MEM
    reg  						i_MemRead;
    reg  						i_MemWrite;
    reg  						i_Signed;
    reg [NB_2-1:0]				i_Long;
    // WB
    reg [NB_2-1:0]				i_MemtoReg;
    reg  						i_RegWrite;

    // OUTPUTS
    wire [NB_INST-1:0]          o_ALUResult;
    wire [NB_INST-1:0]          o_reg_b;
    wire [NB_TREG-1:0]			o_wreg_addr;
    // MEM
    wire  						o_MemRead;
    wire  						o_MemWrite;
    wire  						o_Signed;
    wire [NB_2-1:0]				o_Long;
    // WB
    wire [NB_2-1:0]				o_MemtoReg;
    wire  						o_RegWrite;
    wire [NB_INST-1:0]			o_pc;

    initial begin
        #10
        i_clk 		= 1'b0;
        i_reset 	= 1'b1;
        i_pc 		= 32'b100;
        i_reg_a 	= 32'd15;
        i_reg_b		= 32'd4;
        i_shamt		= 5'd2;
        i_reg_rt	= 5'd10;
        i_reg_rd 	= 5'd20;
        // EX
        i_ALUOp		= RTYPE;
        i_ALUSrc	= 1'b0;
        i_RegDst	= 2'b00;
        i_Jr		= 1'b0;
        // MEM
        i_MemRead 	= 1'b1;
        i_MemWrite 	= 1'b0;
        i_Signed	= 1'b0;
        i_Long		= 2'b10;
        // WB
        i_MemtoReg	= 2'b01;
        i_RegWrite	= 1'b0;

        #10
        i_reset = 1'b0;
        i_signext	= {24'b0,SLL};


       	#20
        // SLL
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd4 << 5'd2))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end
    	
        // SRL
        i_signext	= {24'b0,SRL};
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd4 >> 5'd2))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end
    	
    	// SRA
        i_signext	= {24'b0,SRA};
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd4 >>> 5'd2))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end
    	
    	// SLLV
        i_signext	= {24'b0,SLLV};
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd4 << 5'd15))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end
    	
    	// SRLV
        i_signext	= {24'b0,SRLV};
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd4 >> 5'd15))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end
    	
    	// SRAV
        i_signext	= {24'b0,SRAV};
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd4 >>> 5'd15))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end
    	
    	// ADDU
        i_signext	= {24'b0,ADDU};
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd15 + 32'd4))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end
    	
    	// SUBU
        i_signext	= {24'b0,SUBU};
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd15 - 32'd4))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end
    	
    	// AND
        i_signext	= {24'b0,AND};
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd15 & 32'd4))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end
    	
    	// OR
        i_signext	= {24'b0,OR};
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd15 | 32'd4))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end
    	
    	// XOR
        i_signext	= {24'b0,XOR};
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd15 ^ 32'd4))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end
    	
    	// NOR
        i_signext	= {24'b0,NOR};
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != ~(32'd15 | 32'd4))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end
    	
    	// SLT
        i_signext	= {24'b0,SLT};
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd15 < 32'd4))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end
    	
    	// JALR (JR = 0)
        i_signext	= {24'b0,JALR};
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd15 + 32'd1))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end

    	// JALR (JR = 1)
    	i_Jr 		= 1'b1;
        i_signext	= {24'b0,JALR};
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd4 + 32'd1))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end

    	// LOADS
    	i_Jr 		= 1'b0;
    	i_signext	= 32'd10;
    	i_ALUOp		= LOADS;
    	i_ALUSrc	= 1'b1;
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd15 + 32'd10))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end

    	// ANDI
    	i_ALUOp		= ANDI;
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd15 & 32'd10))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end

    	// ORI
    	i_ALUOp		= ORI;
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd15 | 32'd10))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end

    	// XORI
    	i_ALUOp		= XORI;
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd15 ^ 32'd10))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end

    	// LUI
    	i_ALUOp		= LUI;
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != $signed(32'd10 << 16))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end

    	// SLTI
    	i_ALUOp		= SLTI;
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd15 < 32'd10))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end

    	// BEQ
    	i_ALUOp		= BEQ;
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10) ||
        	(o_ALUResult != (32'd15 - 32'd10))) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end

    	// RegDst = RT
    	i_RegDst	= 2'b00;
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd10)) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end

    	// RegDst = RD
    	i_RegDst	= 2'b01;
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd20)) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end

    	// RegDst = 31
    	i_RegDst	= 2'b10;
    	#20
        if ((o_reg_b != 32'd4) || (o_wreg_addr != 5'd31)) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end

		#100
        
        $display("############# Test OK ############");
        $finish();
    end

    // CLOCK_GENERATION
    always #20 i_clk = ~i_clk;

    EX
    #(
    	.NB_INST     		(NB_INST),
       	.NB_OPCODE        	(NB_OPCODE),
       	.NB_TREG        	(NB_TREG),
       	.NB_OPER        	(NB_OPER),
       	.NB_ALUOP           (NB_ALUOP),
       	.NB_2           	(NB_2)
	)
	u_EX
	(	
		// INPUTS
		.i_ALUOp			(i_ALUOp),
		.i_ALUSrc 			(i_ALUSrc),
		.i_RegDst			(i_RegDst),
		.i_reg_a 			(i_reg_a),
		.i_reg_b			(i_reg_b),
		.i_signext 			(i_signext),
		.i_shamt			(i_shamt),
		.i_reg_rt			(i_reg_rt),
		.i_reg_rd 			(i_reg_rd),
		.i_Jr				(i_Jr),
		// MEM
		.i_MemRead			(i_MemRead),
		.i_MemWrite			(i_MemWrite),
		.i_Signed			(i_Signed),
		.i_Long				(i_Long),
		// WB
		.i_MemtoReg			(i_MemtoReg),
		.i_RegWrite			(i_RegWrite),
		.i_pc				(i_pc),

		// OUTPUTS
		.o_ALUResult		(o_ALUResult),
		.o_reg_b			(o_reg_b),
		.o_wreg_addr		(o_wreg_addr),
		// MEM
		.o_MemRead			(o_MemRead),
		.o_MemWrite			(o_MemWrite),
		.o_Signed			(o_Signed),
		.o_Long				(o_Long),
		// WB
		.o_MemtoReg			(o_MemtoReg),
		.o_RegWrite			(o_RegWrite),
		.o_pc				(o_pc)
	);
 
endmodule
