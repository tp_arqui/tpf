`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/23/2021 02:28:03 PM
// Design Name: 
// Module Name: mips
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mips
#(
	parameter NB_INST		= 	32,     	// Tamaño de instruccion
	parameter NB_ADDR 		= 	26,			// Longitud de la dir. inmediata (J|JAL)
    parameter NB_INMED		=   16,         // Longitud del inmediato
    parameter RAM_SIZE		= 	10,    		// Tamanio de la memoria, 1 MB
    parameter NB_OPCODE		=	6,			// Longitud del OPCODE
    parameter NB_TREG		=   5,	   		// Longitud del campo RS,RT,RD
    parameter NB_OPER		=   4,			// Longitud del operando de ALU
    parameter NB_ALUOP		=	3,			// Longitud del ALUOP
    parameter NB_2          =   2
)
(
    //INPUTS
    input wire                      i_clk,
    input wire                      i_reset,
    input wire                      i_write,        // Indica si se escribe la memoria de instrucciones   
    input wire                      i_enable,       // Para detener el PC y por lo tanto detener la etapa.    
    input wire 	[NB_INST-1:0]       i_instruction,  // Instruccion para escribir en memoria
    input wire 	[NB_INST-1:0]       i_address,      // Direccion para escribir en memoria
    
    //OUTPUTS
    output wire [NB_INST-1:0]     	o_result      	// Instruccion leida de la memoria.
);

	// INTERNAL
	// IF_INPUTS
    wire                    Branch_IF;       	// Para salto condicional, sumando la parte baja de la instruccion al PC. 
    wire                    Jump_IF;         	// Para salto incondicional, leyendo direccion desde parte baja de la instruccion.  
    wire                    Jr;           		// Para salto incondicional, leyendo nueva direccion desde registro. 
    wire [NB_INST-1:0]      jr_addr_IF;      	// Para salto JR. Necesito la direccion desde el registro 31
    wire [NB_INST-1:0]      jump_addr_IF;    	// Para salto incondicional. Necesito nueva direccion a donde saltar.
    wire [NB_INST-1:0]      branch_addr_IF;  	// Para salto condicional. Necesito nueva direccion a donde saltar.
    // IF_ID_INPUTS
    wire                   	stall;
    wire                    flush = (Branch_IF || Jump_IF || Jr);
	wire                    Halt;
	wire [NB_INST-1:0]		instruction_IF_ID;     
    wire [NB_INST-1:0]		pc_IF_ID;
    // ID_INPUTS
    wire [NB_INST-1:0]      instruction_ID;
    wire [NB_INST-1:0]		pc_ID;
    wire [NB_TREG-1:0]		wreg_addr_ID;
    wire [NB_INST-1:0]		wreg_data_ID;
    wire                    RegWrite_ID;
    wire [NB_2-1:0]			forwardA_Branch;
    wire [NB_2-1:0]			forwardB_Branch;
    // ID_EX_INPUTS
    wire [NB_ALUOP-1:0]		ALUOp_ID_EX;
    wire					ALUSrc_ID_EX;						
    wire [NB_2-1:0]			RegDst_ID_EX;
    wire [NB_INST-1:0]		reg_a_ID_EX;		
    wire [NB_INST-1:0]		reg_b_ID_EX;			
    wire [NB_INST-1:0]		signext_ID_EX;
    wire [NB_TREG-1:0]		shamt_ID_EX;
    wire [NB_TREG-1:0]		reg_rs_ID_EX;
    wire [NB_TREG-1:0]		reg_rt_ID_EX;
    wire [NB_TREG-1:0]		reg_rd_ID_EX;
    wire					Jr_ID_EX;
    wire					MemRead_ID_EX;
    wire					MemWrite_ID_EX;
    wire					Signed_ID_EX;
    wire [NB_2-1:0]	    	Long_ID_EX;
    wire [NB_2-1:0]			MemtoReg_ID_EX;	 
    wire                    RegWrite_ID_EX;
    wire [NB_INST-1:0]		pc_ID_EX;
    // EX_INPUTS
    wire [NB_ALUOP-1:0]		ALUOp_EX;
    wire					ALUSrc_EX;						
    wire [NB_2-1:0]			RegDst_EX;
    wire [NB_INST-1:0]		reg_a_EX;		
    wire [NB_INST-1:0]		reg_b_EX;			
    wire [NB_INST-1:0]		signext_EX;
    wire [NB_TREG-1:0]		shamt_EX;
    wire [NB_TREG-1:0]		reg_rs_EX;
    wire [NB_TREG-1:0]		reg_rt_EX;
    wire [NB_TREG-1:0]		reg_rd_EX;
    wire					Jr_EX;
    wire					MemRead_EX;
    wire					MemWrite_EX;
    wire					Signed_EX;
    wire [NB_2-1:0]	    	Long_EX;
    wire [NB_2-1:0]			MemtoReg_EX;	 
    wire                    RegWrite_EX;
    wire [NB_INST-1:0]		pc_EX;	
    wire [NB_2-1:0]			forwardA;
    wire [NB_2-1:0]			forwardB;
    // EX_MEM_INPUTS		
    wire [NB_INST-1:0]      ALUResult_EX_MEM;
    wire [NB_INST-1:0]      reg_b_EX_MEM;
    wire [NB_TREG-1:0]		wreg_addr_EX_MEM;
    wire  					MemRead_EX_MEM;
    wire  					MemWrite_EX_MEM;
    wire  					Signed_EX_MEM;
    wire [NB_2-1:0]			Long_EX_MEM;
    wire [NB_2-1:0]			MemtoReg_EX_MEM;
    wire  					RegWrite_EX_MEM;
    wire [NB_INST-1:0]		pc_EX_MEM;
    // MEM_INPUTS
    wire [NB_INST-1:0]      ALUResult_MEM;
    wire [NB_INST-1:0]      reg_b_MEM;
    wire  					MemRead_MEM;
    wire  					MemWrite_MEM;
    wire  					Signed_MEM;
    wire [NB_2-1:0]			Long_MEM;
    // MEM_WB_INPUTS
    wire [NB_INST-1:0]		DataRead_MEM_WB;
    wire [NB_TREG-1:0]		wreg_addr_MEM_WB;
    wire  					RegWrite_MEM_WB;
    wire [NB_2-1:0]			MemtoReg_MEM_WB;
    wire [NB_INST-1:0]		pc_MEM_WB;
    // WB_INPUTS
    wire [NB_INST-1:0]		DataRead_WB;
    wire [NB_INST-1:0]      ALUResult_WB;
    wire [NB_TREG-1:0]		wreg_addr_WB;
    wire  					RegWrite_WB;
    wire [NB_2-1:0]			MemtoReg_WB;
    wire [NB_INST-1:0]		pc_WB;
    wire                    enable_IF;                    
    // OUTPUT
    assign o_result = wreg_data_ID;
    assign enable_IF = (~stall && i_enable);
    HazardUnit
    #(
        .NB_TREG            (NB_TREG)
	)
	u_HazardUnit
	(
		.i_reg_rs_ID		(reg_rs_ID_EX),
		.i_reg_rt_ID		(reg_rt_ID_EX),
		.i_reg_rt_EX		(reg_rt_EX),
		.i_MemRead_EX		(MemRead_EX),
		.o_stall			(stall)
	);

    ForwardingUnit
    #(
        .NB_TREG            (NB_TREG),
        .NB_2               (NB_2)
    )
    u_ForwardingUnit
    (
    	.i_reg_rs			(reg_rs_EX),
    	.i_reg_rt			(reg_rt_EX),
    	.i_wreg_addr_EX_MEM	(wreg_addr_MEM_WB),
    	.i_wreg_addr_MEM_WB	(wreg_addr_WB),
    	.i_RegWrite_EX_MEM	(RegWrite_MEM_WB),
    	.i_RegWrite_MEM_WB	(RegWrite_WB),
    	.o_forwardA			(forwardA),
    	.o_forwardB			(forwardB)
    );

    ForwardingUnit_Branch
    #(
        .NB_TREG            (NB_TREG),
        .NB_2               (NB_2)
    )
    u_ForwardingUnit_Branch
    (
    	.i_reg_rs			(reg_rs_ID_EX),
    	.i_reg_rt			(reg_rt_ID_EX),
    	.i_wreg_addr_ID_EX	(wreg_addr_EX_MEM),
    	.i_wreg_addr_EX_MEM	(wreg_addr_MEM_WB),
    	.i_wreg_addr_MEM_WB	(wreg_addr_WB),
    	.i_RegWrite_ID_EX	(RegWrite_EX_MEM),
    	.i_RegWrite_EX_MEM	(RegWrite_MEM_WB),
    	.i_RegWrite_MEM_WB	(RegWrite_WB),
    	.o_forwardA			(forwardA_Branch),
    	.o_forwardB			(forwardB_Branch)
    );

    WB
    #(
        .NB_INST            (NB_INST),
        .NB_TREG            (NB_TREG),
        .NB_2               (NB_2)
    )
    u_WB
    (
    	// INPUTS
        .i_wreg_addr        (wreg_addr_WB),
        .i_RegWrite         (RegWrite_WB),
        .i_MemtoReg         (MemtoReg_WB),
        .i_pc 	            (pc_WB),
        .i_DataRead         (DataRead_WB),
        .i_ALUResult        (ALUResult_WB),

        // OUTPUTS
        .o_wreg_addr		(wreg_addr_ID),
        .o_RegWrite 		(RegWrite_ID),
        .o_wreg_data		(wreg_data_ID)
    );

	MEM_WB
	#(
	    .NB_INST            (NB_INST),
	    .NB_TREG            (NB_TREG),
	    .NB_2               (NB_2)  
	)
	u_MEM_WB
	(	
		// INPUTS
	    .i_clk              (i_clk),
	    .i_reset			(i_reset),
	    .i_enable			(i_enable),
	    .i_ALUResult        (ALUResult_MEM),
	    .i_DataRead         (DataRead_MEM_WB),
	    .i_wreg_addr        (wreg_addr_MEM_WB),
	    .i_MemtoReg         (MemtoReg_MEM_WB),
	    .i_RegWrite			(RegWrite_MEM_WB),
	    .i_pc 				(pc_MEM_WB),

	    // OUTPUTS
	    .o_ALUResult        (ALUResult_WB),
	    .o_DataRead         (DataRead_WB),
	    .o_wreg_addr        (wreg_addr_WB),
	    .o_MemtoReg         (MemtoReg_WB),
	    .o_RegWrite			(RegWrite_WB),
	    .o_pc 				(pc_WB)
	);

	MEM
	#(
	    .NB_2               (NB_2),    
	    .NB_DATA            (NB_INST),
	    .NB_ADDR            (NB_INST),
	    .RAM_SIZE           (RAM_SIZE)
	)
	u_MEM
	(
	    .i_clk              (i_clk),
	    .i_Addr             (ALUResult_MEM),
	    .i_WriteData        (reg_b_MEM),
	    .i_MemRead          (MemRead_MEM),
	    .i_MemWrite         (MemWrite_MEM),
	    .i_Signed           (Signed_MEM),
	    .i_Long             (Long_MEM),
	    .o_ReadData         (DataRead_MEM_WB)
	);

	EX_MEM
    #(
    	.NB_INST     		(NB_INST),
       	.NB_TREG        	(NB_TREG),
       	.NB_2           	(NB_2)
	)
	u_EX_MEM
	(
		// INPUTS
		.i_clk				(i_clk),
		.i_reset			(i_reset),
		.i_enable			(i_enable),
		.i_ALUResult		(ALUResult_EX_MEM),
		.i_reg_b			(reg_b_EX_MEM),
		.i_wreg_addr		(wreg_addr_EX_MEM),
		// MEM
		.i_MemRead			(MemRead_EX_MEM),
		.i_MemWrite			(MemWrite_EX_MEM),
		.i_Signed			(Signed_EX_MEM),
		.i_Long				(Long_EX_MEM),
		// WB
		.i_MemtoReg			(MemtoReg_EX_MEM),
		.i_RegWrite			(RegWrite_EX_MEM),
		.i_pc				(pc_EX_MEM),

		// OUTPUTS
		.o_ALUResult		(ALUResult_MEM),
		.o_reg_b			(reg_b_MEM),
		.o_wreg_addr		(wreg_addr_MEM_WB),
		// MEM
		.o_MemRead			(MemRead_MEM),
		.o_MemWrite			(MemWrite_MEM),
		.o_Signed			(Signed_MEM),
		.o_Long				(Long_MEM),
		// WB
		.o_MemtoReg			(MemtoReg_MEM_WB),
		.o_RegWrite			(RegWrite_MEM_WB),
		.o_pc				(pc_MEM_WB)
	);

	EX
    #(
    	.NB_INST     		(NB_INST),
       	.NB_OPCODE        	(NB_OPCODE),
       	.NB_TREG        	(NB_TREG),
       	.NB_OPER        	(NB_OPER),
       	.NB_ALUOP           (NB_ALUOP),
       	.NB_2           	(NB_2)
	)
	u_EX
	(	
		// INPUTS
		.i_ALUOp			(ALUOp_EX),
		.i_ALUSrc 			(ALUSrc_EX),
		.i_RegDst			(RegDst_EX),
		.i_reg_a 			(reg_a_EX),
		.i_reg_b			(reg_b_EX),
		.i_signext 			(signext_EX),
		.i_shamt			(shamt_EX),
		.i_reg_rs			(reg_rs_EX),
		.i_reg_rt			(reg_rt_EX),
		.i_reg_rd 			(reg_rd_EX),
		.i_Jr				(Jr_EX),
		.i_forwardA 		(forwardA),
		.i_forwardB			(forwardB),
		// MEM
		.i_MemRead			(MemRead_EX),
		.i_MemWrite			(MemWrite_EX),
		.i_Signed			(Signed_EX),
		.i_Long				(Long_EX),
		.i_reg_MEM 			(ALUResult_MEM),
		// WB
		.i_MemtoReg			(MemtoReg_EX),
		.i_RegWrite			(RegWrite_EX),
		.i_pc				(pc_EX),
		.i_reg_WB 			(wreg_data_ID),

		// OUTPUTS
		.o_ALUResult		(ALUResult_EX_MEM),
		.o_reg_b			(reg_b_EX_MEM),
		.o_wreg_addr		(wreg_addr_EX_MEM),
		// MEM
		.o_MemRead			(MemRead_EX_MEM),
		.o_MemWrite			(MemWrite_EX_MEM),
		.o_Signed			(Signed_EX_MEM),
		.o_Long				(Long_EX_MEM),
		// WB
		.o_MemtoReg			(MemtoReg_EX_MEM),
		.o_RegWrite			(RegWrite_EX_MEM),
		.o_pc				(pc_EX_MEM)
	);

	ID_EX
    #(
    	.NB_INST     		(NB_INST),
       	.NB_TREG        	(NB_TREG),
       	.NB_ALUOP           (NB_ALUOP),
       	.NB_2           	(NB_2)
    )
    u_ID_EX
    (
    	// INPUTS
    	.i_clk				(i_clk),
    	.i_reset			(i_reset),
    	.i_enable			(i_enable),
    	.i_stall			(stall),
		.i_ALUOp			(ALUOp_ID_EX),
		.i_ALUSrc			(ALUSrc_ID_EX),
		.i_RegDst			(RegDst_ID_EX),
    	.i_reg_a 			(reg_a_ID_EX),
		.i_reg_b			(reg_b_ID_EX),
		.i_signext			(signext_ID_EX),
		.i_shamt			(shamt_ID_EX),
		.i_reg_rs			(reg_rs_ID_EX),
		.i_reg_rt			(reg_rt_ID_EX),
		.i_reg_rd			(reg_rd_ID_EX),
		.i_Jr 				(Jr),
		// MEM
		.i_MemRead			(MemRead_ID_EX),
		.i_MemWrite			(MemWrite_ID_EX),
		.i_Signed			(Signed_ID_EX),
		.i_Long				(Long_ID_EX),
		// WB
		.i_MemtoReg			(MemtoReg_ID_EX),
        .i_RegWrite         (RegWrite_ID_EX),
        .i_pc				(pc_ID_EX),

        // OUTPUTS
		.o_ALUOp			(ALUOp_EX),
		.o_ALUSrc			(ALUSrc_EX),
		.o_RegDst			(RegDst_EX),
    	.o_reg_a 			(reg_a_EX),
		.o_reg_b			(reg_b_EX),
		.o_signext			(signext_EX),
		.o_shamt			(shamt_EX),
		.o_reg_rs			(reg_rs_EX),
		.o_reg_rt			(reg_rt_EX),
		.o_reg_rd			(reg_rd_EX),
		.o_Jr 				(Jr_EX),
		// MEM
		.o_MemRead			(MemRead_EX),
		.o_MemWrite			(MemWrite_EX),
		.o_Signed			(Signed_EX),
		.o_Long				(Long_EX),
		// WB
		.o_MemtoReg			(MemtoReg_EX),
        .o_RegWrite         (RegWrite_EX),
        .o_pc				(pc_EX)	
    );

  	ID
    #(
    	.NB_INST     		(NB_INST),
       	.NB_ADDR          	(NB_ADDR),
       	.NB_INMED	        (NB_INMED),
       	.NB_OPCODE        	(NB_OPCODE),
       	.NB_TREG        	(NB_TREG),
       	.NB_ALUOP           (NB_ALUOP),
       	.NB_2           	(NB_2)
	)
	u_ID
	(
		.i_clk				(i_clk),
		.i_reset			(i_reset),
		.i_RegWrite			(RegWrite_ID),
		.i_instruction		(instruction_ID),
		.i_pc				(pc_ID),
		.i_wreg_addr		(wreg_addr_ID),
		.i_wreg_data		(wreg_data_ID),
		.i_forwardA_Branch	(forwardA_Branch),
		.i_forwardB_Branch	(forwardB_Branch),
		.i_reg_EX 			(ALUResult_EX_MEM),
		.i_reg_MEM 			(ALUResult_MEM),
		.i_reg_WB 			(wreg_data_ID),
		// FETCH
		.o_jump_addr		(jump_addr_IF),
		.o_branch_addr		(branch_addr_IF),
		.o_jr_addr			(jr_addr_IF),
		.o_Branch 			(Branch_IF),
		.o_Jump 			(Jump_IF),
		.o_Jr 				(Jr),
		// EX
		.o_reg_a 			(reg_a_ID_EX),
		.o_reg_b			(reg_b_ID_EX),
		.o_signext			(signext_ID_EX),
		.o_RegDst			(RegDst_ID_EX),
		.o_ALUOp			(ALUOp_ID_EX),
		.o_ALUSrc			(ALUSrc_ID_EX),
		.o_reg_rs			(reg_rs_ID_EX),
		.o_reg_rt			(reg_rt_ID_EX),
		.o_reg_rd			(reg_rd_ID_EX),
		.o_shamt			(shamt_ID_EX),
		.o_pc 				(pc_ID_EX),
		// MEM
		.o_MemRead			(MemRead_ID_EX),
		.o_MemWrite			(MemWrite_ID_EX),
		.o_Signed			(Signed_ID_EX),
		.o_Long				(Long_ID_EX),
		// WB
		.o_MemtoReg			(MemtoReg_ID_EX),
        .o_RegWrite         (RegWrite_ID_EX),
		.o_Halt				(Halt)
	); 	

	IF_ID
	#(
	    .NB_ADDR                (NB_INST),
	    .NB_INST                (NB_INST)
	) 
	u_IF_ID
	(
	    .i_clk                  (i_clk),
	    .i_reset                (i_reset),
	    .i_enable				(i_enable),
	    .i_stall                (stall),
	    .i_flush                (flush),
	    .i_Halt					(Halt),
	    .i_instruction          (instruction_IF_ID),
	    .i_pc_sum               (pc_IF_ID),
	    .o_instruction          (instruction_ID),
	    .o_pc_sum               (pc_ID)
	);

	IF
	#(
	    .NB_ADDR                (NB_INST),
	    .NB_INST                (NB_INST)   
	)
	u_IF
	(
	    .i_clk                  (i_clk),
	    .i_reset                (i_reset),
	    .i_write                (i_write),
	    .i_enable               (enable_IF),
	    .i_Branch               (Branch_IF),
	    .i_Jump                 (Jump_IF),
	    .i_Jr                   (Jr),
	    .i_jr_addr              (jr_addr_IF),
	    .i_jump_addr            (jump_addr_IF),
	    .i_branch_addr          (branch_addr_IF),
	    .i_instruction          (i_instruction),
	    .i_address              (i_address),
	    .o_instruction          (instruction_IF_ID),
	    .o_pc_sum               (pc_IF_ID)
	);

endmodule
