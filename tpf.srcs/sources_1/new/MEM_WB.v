`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/21/2021 04:39:14 PM
// Design Name: 
// Module Name: MEM_WB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MEM_WB
#(
	parameter               NB_INST         =   32,         // Longitud del registro
    parameter				NB_TREG		   	=   5,	   		// Longitud del campo RS,RT,RD
    parameter               NB_2            =   2
)
(
	// INPUTS
	input wire					           	i_clk,
    input wire					           	i_reset,
    input wire                              i_enable,

    input wire [NB_INST-1:0]               	i_ALUResult,
    input wire [NB_INST-1:0]               	i_DataRead,
    input wire [NB_TREG-1:0]				i_wreg_addr,

    // WB
    input wire 	[NB_2-1:0]					i_MemtoReg,
    input wire  							i_RegWrite,
    input wire 	[NB_INST-1:0]				i_pc,
    
    // OUTPUTS
	output wire [NB_INST-1:0]               o_ALUResult,
    output wire [NB_INST-1:0]               o_DataRead,
    output wire [NB_TREG-1:0]				o_wreg_addr,

    // WB
    output wire [NB_2-1:0]					o_MemtoReg,
    output wire  							o_RegWrite,
    output wire [NB_INST-1:0]				o_pc
);

	// INTERNAL
    reg [NB_INST-1:0]           ALUResult,	ALUResult_next;
    reg [NB_INST-1:0]           DataRead,	DataRead_next;
    reg [NB_TREG-1:0]			wreg_addr,	wreg_addr_next;

    // WB
    reg [NB_2-1:0]				MemtoReg,	MemtoReg_next;
    reg  						RegWrite,	RegWrite_next;
    reg [NB_INST-1:0]			pc,			pc_next;

	always @(posedge i_clk) begin 
		if(i_reset) begin
			ALUResult	<= {NB_INST{1'b0}};
			DataRead	<= {NB_INST{1'b0}};
		    wreg_addr	<= {NB_TREG{1'b0}};
			MemtoReg 	<= {NB_2{1'b0}};
		    RegWrite 	<= 1'b0;
		    pc 			<= {NB_INST{1'b0}};
		end else begin                    //Funcionamiento normal del latch EX_MEM
			if (i_enable) begin
				ALUResult	<= ALUResult_next;
				DataRead 	<= DataRead_next;
			    wreg_addr	<= wreg_addr_next;
				MemtoReg 	<= MemtoReg_next;
			    RegWrite 	<= RegWrite_next;
			    pc 			<= pc_next;
		    end
		end
	end

	always @(*) begin
		ALUResult_next	<= i_ALUResult;
		DataRead_next 	<= i_DataRead;
	    wreg_addr_next	<= i_wreg_addr;
		MemtoReg_next 	<= i_MemtoReg;
	    RegWrite_next 	<= i_RegWrite;
	    pc_next			<= i_pc;	
    end
        
    // OUTPUT
    assign o_ALUResult	= ALUResult;
	assign o_DataRead	= DataRead;
	assign o_wreg_addr	= wreg_addr;
	assign o_MemtoReg 	= MemtoReg;
    assign o_RegWrite 	= RegWrite;
    assign o_pc 		= pc;

endmodule