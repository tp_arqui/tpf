`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/24/2021 02:18:56 PM
// Design Name: 
// Module Name: ForwardingUnit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ForwardingUnit
#(
	parameter NB_TREG		=   5,	   		// Longitud del campo RS,RT,RD
    parameter NB_2          =   2
)
(
    // INPUTS
    input wire 	[NB_TREG-1:0]       i_reg_rs,
    input wire 	[NB_TREG-1:0]       i_reg_rt,
    input wire 	[NB_TREG-1:0]       i_wreg_addr_EX_MEM,
    input wire 	[NB_TREG-1:0]       i_wreg_addr_MEM_WB,
    input wire 				        i_RegWrite_EX_MEM,
    input wire 				        i_RegWrite_MEM_WB,
    
    // OUTPUTS
    output wire [NB_2-1:0]        	o_forwardA,       // Salida Forwarding A
    output wire [NB_2-1:0]          o_forwardB        // Salida Forwarding B
);
    
	// LOCALPARAMETERS
	localparam                      DEF	= 2'b00;
	localparam                      MEM = 2'b01;
	localparam                      WB  = 2'b10;

    // INTERNAL
    reg [NB_2-1:0]           		forwardA;
    reg [NB_2-1:0]           		forwardB;
    
    always @(*) begin
    	// FORWARD A
        // MEM
        if (i_RegWrite_EX_MEM && (i_wreg_addr_EX_MEM == i_reg_rs))
            forwardA = MEM;        
        // WB
        else if (i_RegWrite_MEM_WB && (i_wreg_addr_MEM_WB == i_reg_rs))
            forwardA = WB;
        else
            forwardA = DEF;
            
        // FORWARD A
        // MEM
		if (i_RegWrite_EX_MEM && (i_wreg_addr_EX_MEM == i_reg_rt))
            forwardB = MEM;
        // WB
		else if (i_RegWrite_MEM_WB && (i_wreg_addr_MEM_WB == i_reg_rt))
            forwardB = WB;
		else 
            forwardB = DEF;
    end

    // OUTPUT
    assign o_forwardA = forwardA;
    assign o_forwardB = forwardB;
    
endmodule