`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/05/2021 07:33:46 PM
// Design Name: 
// Module Name: IF_ID
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module IF
#(
    parameter NB_ADDR       = 32,     //Tamaño de la memoria de instrucciones 1MB
    parameter NB_INST       = 32     //Tamaño de instruccion
)
(
    //INPUTS
    input wire                      i_clk,
    input wire                      i_reset,
    input wire                      i_write,         //Indica si se escribe la memoria de instrucciones   
    input wire                      i_enable,        //Para detener el PC y por lo tanto detener la etapa.    
    input wire                      i_Branch,        //Para salto condicional, sumando la parte baja de la instruccion al PC. 
    input wire                      i_Jump,          //Para salto incondicional, leyendo direccion desde parte baja de la instruccion.  
    input wire                      i_Jr,            //Para salto incondicional, leyendo nueva direccion desde registro. 
    input wire [NB_ADDR-1:0]        i_jr_addr,       //Para salto JR. Necesito la direccion desde el registro 31
    input wire [NB_ADDR-1:0]        i_jump_addr,     //Para salto incondicional. Necesito nueva direccion a donde saltar.
    input wire [NB_ADDR-1:0]        i_branch_addr,   //Para salto condicional. Necesito nueva direccion a donde saltar.
    input wire [NB_INST-1:0]        i_instruction,   //Instruccion para escribir en memoria
    input wire [NB_ADDR-1:0]        i_address,       //Direccion para escribir en la memoria
    
    //OUTPUTS
    output wire [NB_INST-1 : 0]     o_instruction,      //Instruccion leida de la memoria.
    output wire [NB_ADDR-1 : 0]     o_pc_sum            //Proxima direccion a leer de memoria de inst. PC + 1
);

    //INTERNALS
    wire [NB_ADDR - 1:0]  pc_next;         //Direcccion para la siguiente instruccion
    wire [NB_ADDR - 1:0]  pc;              //Direccion de la instruccion actual
    wire [NB_ADDR - 1:0]  mux_branch;      //Direccion de la instruccion dependiendo si es branch o no.
                                             //Puede ser pc_next o la direccion calculada en ID para el branch
    wire [NB_ADDR - 1:0]  mux_jump;        //Direccion de la instruccion dependiendo si es jump o no.
                                             //Puede ser la salida del mux_branch o la direccion calculada en ID para el jump                                             
    wire [NB_ADDR - 1:0]  mux_jr;          //Direccion de la instruccion dependiendo si es jr o no.
                                             //Puede ser la salida del mux_jump o la direccion calculada en ID para el jr proveniente de un reg                                             
    wire [NB_INST - 1:0]  instruction;     //Instruccion actual
    
    wire [NB_ADDR - 1:0]  address;         //Direcccion a leer de memoria

    //OUTPUTS
    assign   o_instruction = instruction;
    assign   o_pc_sum = pc_next;
    
    IF_pc
    #(
        .NB_ADDR                 (NB_ADDR)
    )
    u_IF_pc
    (
        .i_clk                      (i_clk),
        .i_enable                   (i_enable),             //Detiene el contador de pograma. Por ej, cuando hay un halt.
        .i_reset                    (i_reset),
        .i_pc                       (mux_jr),               //La direccion de la instruccion a cargar en el proximo clk, despues de haber pasado por los mux
        .o_pc                       (pc)                    //La direccion de la instruccion actual
     );
     
     IF_sum
     #(
        .NB_ADDR                 (NB_ADDR)
     )
     u_IF_sum
     (
        .i_pc                       (pc),                   //La direccion de la instruccion actual
        .o_pc_add                   (pc_next)               //La direccion de la proxima instruccion
     );
     
     IF_rom
     #(
        .MEMORY_FILE                (""),
        .NB_ADDR                    (NB_ADDR),
        .NB_INST                    (NB_INST)
     )
     u_IF_rom
     (
        .i_clk                        (i_clk),
        .i_reset                      (i_reset),
        .i_write                      (i_write),
        .i_instruction                (i_instruction),
        .i_address                    (address),
        .o_instruction                (instruction)
     );
    
    //Recibe la direccion PC+1 y la direccion de branch. Y selecciona segun la senial de branch
    IF_mux          
    #(
        .NB_DATA                      (NB_ADDR)
    )
    u_IF_mux_branch
    (
        .i_select                     (i_Branch), 
        .i_signal_1                   (pc_next), 
        .i_signal_2                   (i_branch_addr),
        .o_mux                        (mux_branch)   
    );
    
    //Recibe la direccion de u_IF_mux_branch y la direccion de jump. Y selecciona segun la senial de jump
    IF_mux
    #(
        .NB_DATA                   (NB_ADDR)    
    )
    u_IF_mux_jump
    (
        .i_select                     (i_Jump), 
        .i_signal_1                   (mux_branch), 
        .i_signal_2                   (i_jump_addr),
        .o_mux                        (mux_jump) 
    );
    
    //Recibe la direccion de u_IF_mux_jump y la direccion de jr. Y selecciona segun la senial de i_jr
    IF_mux
    #(
        .NB_DATA                   (NB_ADDR)    
    )
    u_IF_mux_jr
    (
        .i_select                     (i_Jr), 
        .i_signal_1                   (mux_jump), 
        .i_signal_2                   (i_jr_addr),
        .o_mux                        (mux_jr) 
    ); 
    
    //Recibe la direccion de PC y una direccion externa. Selecciona segun la senial de i_write
    IF_mux
    #(
        .NB_DATA                   (NB_ADDR)    
    )
    u_IF_mux_PC
    (
        .i_select                     (i_write), 
        .i_signal_1                   (pc), 
        .i_signal_2                   (i_address),
        .o_mux                        (address) 
    );
    
endmodule
