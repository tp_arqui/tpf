`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/15/2021 09:39:22 PM
// Design Name: 
// Module Name: EX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module EX
#(
	parameter               NB_INST         =   32,         // Longitud de registro con signo
	parameter				NB_OPCODE		=	6,			// Longitud del OPCODE
	parameter				NB_TREG		   	=   5,	   		// Longitud del campo RT,RD y Shamt
    parameter               NB_OPER         =   4,			// Longitud del operando de ALU
    parameter				NB_ALUOP		=	3,			// Longitud del ALUOP
    parameter               NB_2	        =   2           // Longitud del selector
)
(
	// INPUTS
    input wire  [NB_ALUOP-1:0] 				i_ALUOp,
    input wire  							i_ALUSrc,
    input wire 	[NB_2-1:0]					i_RegDst,
    input wire 	[NB_INST-1:0]               i_reg_a,
    input wire 	[NB_INST-1:0]               i_reg_b,
    input wire 	[NB_INST-1:0]               i_signext,
	input wire 	[NB_TREG-1:0]               i_shamt, 
	input wire 	[NB_TREG-1:0]               i_reg_rs,
	input wire 	[NB_TREG-1:0]               i_reg_rt, 
	input wire 	[NB_TREG-1:0]               i_reg_rd,    
    input wire  							i_Jr,
    input wire  [NB_2-1:0]					i_forwardA,
    input wire  [NB_2-1:0]					i_forwardB,

    // MEM
    input wire  							i_MemRead,
    input wire  							i_MemWrite,
    input wire  							i_Signed,
    input wire 	[NB_2-1:0]					i_Long,
    input wire 	[NB_INST-1:0]               i_reg_MEM,

    // WB
    input wire 	[NB_2-1:0]					i_MemtoReg,
    input wire  							i_RegWrite,
    input wire 	[NB_INST-1:0]				i_pc,
    input wire 	[NB_INST-1:0]               i_reg_WB,
    
    // OUTPUTS
    output wire [NB_INST-1:0]               o_ALUResult,
    output wire [NB_INST-1:0]               o_reg_b,
    output wire [NB_TREG-1:0]				o_wreg_addr,

    // MEM
    output wire  							o_MemRead,
    output wire  							o_MemWrite,
    output wire  							o_Signed,
    output wire 	[NB_2-1:0]				o_Long,

    // WB
    output wire 	[NB_2-1:0]				o_MemtoReg,
    output wire  							o_RegWrite,
    output wire 	[NB_INST-1:0]			o_pc
);

	// INTERNAL 
	wire [NB_OPCODE-1:0]		funct 	= i_signext[5:0];
	wire [NB_INST-1:0]			input1_ALU;
	wire [NB_INST-1:0]			input2_ALU;
	wire [NB_INST-1:0]			input1_MUX;
	wire [NB_INST-1:0]			input2_MUX;
	wire [NB_OPER-1:0] 			oper;

	// OUTPUT
	// MEM
	assign o_MemRead 	= i_MemRead;
	assign o_MemWrite 	= i_MemWrite;
	assign o_Signed		= i_Signed;
	assign o_Long		= i_Long;
	// WB
	assign o_MemtoReg	= i_MemtoReg;
	assign o_RegWrite	= i_RegWrite;
	assign o_pc			= i_pc;
	assign o_reg_b		= i_reg_b; 

	EX_ALU
	#(
		.NB_INST 			(NB_INST),
		.NB_TREG 			(NB_TREG),
		.NB_OPER 			(NB_OPER)
	)
	u_EX_ALU
	(
		.i_data_1			(input1_ALU),
		.i_data_2			(input2_ALU
			),
		.i_Oper 			(oper),
		.i_Shamt 			(i_shamt),
		.o_ALUResult		(o_ALUResult)
	);

	EX_ALUCtrl
	#(
		.NB_OPCODE     		(NB_OPCODE),
		.NB_ALUOP 			(NB_ALUOP),
		.NB_OPER 			(NB_OPER)		
	)
	u_EX_ALUCtrl
	(
		.i_funct 			(funct),
		.i_ALUOp 			(i_ALUOp),
		.o_Oper				(oper)
	);

	EX_mux_RegDst
	#(
		.NB_TREG     		(NB_TREG),
		.NB_SEL 			(NB_2)
	)
	u_EX_mux_RegDst
	(
		.i_a 				(i_reg_rt),
		.i_b 				(i_reg_rd),
		.i_sel 				(i_RegDst),
		.o_mux 				(o_wreg_addr)
	);

	EX_mux_ALUSrc
	#(
		.NB_INST     		(NB_INST)
	)
	u_EX_mux_ALUSrc
	(
		.i_a 				(input2_MUX),
		.i_b 				(i_signext),
		.i_sel 				(i_ALUSrc),
		.o_mux 				(input2_ALU)
	);

	EX_mux_JrControl
	#(
		.NB_INST     		(NB_INST)
	)
	u_EX_mux_JrControl
	(
		.i_a 				(input1_MUX),
		.i_b 				(i_pc),
		.i_sel 				(i_Jr),
		.o_mux 				(input1_ALU)
	);

	EX_mux_A
	#(
		.NB_INST     		(NB_INST),
		.NB_SEL 			(NB_2)
	)
	u_EX_mux_A
	(
		.i_a 				(i_reg_a),
		.i_b 				(i_reg_MEM),
		.i_c 				(i_reg_WB),
		.i_sel 				(i_forwardA),
		.o_mux 				(input1_MUX)
	);

	EX_mux_B
	#(
		.NB_INST     		(NB_INST),
		.NB_SEL 			(NB_2)
	)
	u_EX_mux_B
	(
		.i_a 				(i_reg_b),
		.i_b 				(i_reg_MEM),
		.i_c 				(i_reg_WB),
		.i_sel 				(i_forwardB),
		.o_mux 				(input2_MUX)
	);

endmodule
