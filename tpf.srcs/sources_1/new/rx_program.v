`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/30/2021 06:44:27 PM
// Design Name: 
// Module Name: rx_program
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rx_program
#(
   parameter SB_TICK		= 16,         // Ticks UART
   parameter NB_DATA        = 32,         //Tamaño de las instrucciones
   parameter MEM_SIZE       = 6,          //Tamanio de la memoria de 32 instrucciones
   parameter STEPS          = 5
)
(
    input  wire                            i_clk,
	input  wire                            i_reset,
	input  wire                            i_step_by_step,
	input  wire                            i_btn,
	input  wire                            i_rx,               //Entrada serial 
	output  wire                            o_write_mips,     //Indica cuando se termino de recibir el programa
	output  wire                            o_rx_done,
	output wire  [NB_DATA -1 : 0]          o_instruction,
	output wire  [NB_DATA -1 : 0]          o_address,
	output wire                            o_step               
);

    //LOCALPARAM
    localparam                              HLT = 32'b11111111111111111111111111111111;

    //INTERNAL
    reg        [0:NB_DATA-1]        memory[2**MEM_SIZE-1:0];            // Array de registros de 32 de largo y 32 de ancho. 
    reg        [NB_DATA-1:0]        addr=0, addr_next=0;                // la direccion inicial en 0
    //wire                            
    reg         [0:NB_DATA-1]       instruction;
    wire        [0:NB_DATA-1]       instruction_received;
    reg         [0:NB_DATA-1]       instruction_send;
    reg                             program_received=1'b0;
    reg                             program_running=1'b0;
    wire                            step;
    reg                             step_next = 1'b0;
    reg                             step_to_step = 1'b0;
    reg                             write_mips = 1'b0;
    
    
    always@(posedge i_clk)begin
        if(i_reset)begin
             addr<=0;
             addr_next<=0;
             instruction<=0;
        end
        else begin
            if(program_received && write_mips)begin
                addr<=addr_next;
                instruction_send <= memory[addr];
                if (instruction_send==HLT && write_mips)begin
                    program_received<=1'b0;
                    program_running<=1'b1;
                    write_mips<=1'b0;
                end
            end
            else if(!program_received && o_rx_done) begin
                addr<=addr_next;
                memory[addr] <= instruction;
                if(instruction==HLT)begin               //Lo pongo aca porque sino no terminaba de guardar el dato
                    program_received<=1'b1;
                end
            end
            else if(program_received) begin
                    write_mips<=1'b1;              //Dejo pasar un clk y recien ahi inicio el mips.
                    addr<=0;                     //Reseteo para pasar de la memoria del receptor a la del mips
                    addr_next<=0;
    
            end
        end
    end
    
    always@(posedge i_clk)begin
        step_next <= step_to_step; 
    end
    
    always @(*)begin
        addr_next = addr+1;  //Se actualiza con el clk en el always de arriba
        instruction = instruction_received;
        if(program_running) begin 
            if(i_step_by_step == 1) begin
                 step_to_step = step;
            end
            else begin
                 step_to_step = 1'b1;
            end
        end
   end
   
   assign   o_program_received  = program_received;
   assign   o_instruction       = instruction_send;
   assign   o_write_mips        = write_mips;
   assign   o_step              = step_next;
   assign   o_address           = (addr==0) ? 0 : addr-1;
   
   uart
    #(
        .NB_DATA          (NB_DATA),
        .SB_TICK          (SB_TICK)
    )
    u_uart
    (
        .i_clk            (i_clk),
        .i_reset          (i_reset), 
        .i_rx             (i_rx), 
        .o_rx_done        (o_rx_done),
        .o_rx             (instruction_received)
    );
    
     step u_step
    (
        .i_clk              (i_clk),
        .i_reset            (i_reset),
        .i_btn              (i_btn),
        .o_step             (step)
    );
endmodule