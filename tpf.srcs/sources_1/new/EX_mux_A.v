`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/26/2021 08:50:55 PM
// Design Name: 
// Module Name: EX_mux_A
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module EX_mux_A
#(
    parameter               NB_INST         =   32,         // Longitud de registro con signo
    parameter               NB_SEL          =   2           // Longitud del selector
)
(
    // INPUTS   
    input wire  [NB_INST-1:0]               i_a,
    input wire  [NB_INST-1:0]               i_b,
    input wire  [NB_INST-1:0]               i_c,
    input wire  [NB_SEL -1:0]               i_sel,
    
    // OUTPUTS
    output wire [NB_INST-1:0]               o_mux
);

	// LOCALPARAMETERS
    localparam 			latch  	= 2'b00;                                
    localparam          mem   	= 2'b01;
    localparam          wb  	= 2'b10;
    localparam          def 	= 32'b0;

    // INTERNAL
    reg         [NB_INST-1:0]               mux;

    always @(*) begin : proc_mux
        case (i_sel)
            
            latch	: mux <= i_a;   // RegA -> ID_EX    
            mem		: mux <= i_b;   // RegA -> MEM
            wb		: mux <= i_c;   // RegA -> WB      
            default : mux <= def;   // RegA -> 0
        endcase
    end

    // OUTPUT
    assign o_mux = mux;
endmodule
