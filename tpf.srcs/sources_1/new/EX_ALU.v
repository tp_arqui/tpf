`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/15/2021 09:30:23 PM
// Design Name: 
// Module Name: EX_ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module EX_ALU
#(
	parameter               NB_INST         =   32,         // Longitud de registro con signo
	parameter				NB_TREG		   	=   5,	   		// Longitud del campo Shamt
    parameter               NB_OPER         =   4			// Longitud del operando de ALU
)
(
	// INPUTS   
	input wire	[NB_INST-1:0]				i_data_1,
	input wire	[NB_INST-1:0]				i_data_2,
    input wire 	[NB_OPER-1:0]               i_Oper,
    input wire 	[NB_TREG-1:0]				i_Shamt,
    
    // OUTPUTS
    output wire [NB_INST-1:0]               o_ALUResult
);
	
	// LOCALPARAMETERS
	localparam SLL  = 4'b0000;
	localparam SRL  = 4'b0001;
	localparam SRA  = 4'b0010;
	localparam SLLV = 4'b0011;
	localparam SRLV = 4'b0100;
	localparam SRAV = 4'b0101;
	localparam ADDU = 4'b0110;
	localparam SUBU = 4'b0111;
	localparam AND  = 4'b1000;
	localparam OR   = 4'b1001;
	localparam XOR  = 4'b1010;
	localparam NOR  = 4'b1011;
	localparam SLT  = 4'b1100;
	localparam JALR = 4'b1101;
	localparam LUI	= 4'b1110;


	// INTERNAL
	reg [NB_INST-1:0] 						ALUResult;

	always @(*) begin : proc_ALU

		case (i_Oper)

			SLL  : ALUResult =   i_data_2 <<  i_Shamt;
			SRL  : ALUResult =   i_data_2 >>  i_Shamt;
			SRA  : ALUResult =   i_data_2 >>> i_Shamt;
			SLLV : ALUResult =   i_data_2 <<  i_data_1[4:0];
			SRLV : ALUResult =   i_data_2 >>  i_data_1[4:0];
			SRAV : ALUResult =   i_data_2 >>> i_data_1[4:0];
			ADDU : ALUResult =   i_data_1  +  i_data_2;
			SUBU : ALUResult =   i_data_1  -  i_data_2;
			AND  : ALUResult =   i_data_1  &  i_data_2;
			OR   : ALUResult =   i_data_1  |  i_data_2;
			XOR  : ALUResult =   i_data_1  ^  i_data_2;
			NOR  : ALUResult = ~(i_data_1  |  i_data_2);
			SLT  : ALUResult =   i_data_1  <  i_data_2 ? 32'b1 : 32'b0;
			JALR : ALUResult =   i_data_1  +  1'b1;
			LUI  : ALUResult = 	 $signed(i_data_2 << 32'd16);

			default : ALUResult = 32'b0;

		endcase
	end

	// OUTPUT
	assign o_ALUResult = ALUResult;

endmodule