`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/16/2021 11:31:39 AM
// Design Name: 
// Module Name: MEM_data_memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MEM_data_memory
#(
    parameter				   NB_2			= 	2,        
    parameter				   NB_DATA		= 	32,     //Tamanio del dato
    parameter				   NB_ADDR		= 	32,     //Tamanio de la direccion, puede direccionar hasta 4GB
    parameter				   RAM_SIZE		= 	10,     //Tamanio de la memoria, 1 MB
    parameter                  DATA_FILE    =   ""      //Nombre del archivo para cargar en la memoria de datos. 
)
(   
    // INPUTS
    input wire                              i_clk,          
    input wire                              i_MemWrite,     //Senial para escribir la memoria
    input wire                              i_MemRead,      //Senial para leer la memoria
    input wire                              i_Signed,       //Senial que indica si 
    input wire [NB_ADDR-1:0]                i_Addr,
    input wire [NB_2-1:0]                   i_Long,
    input wire [NB_DATA-1:0]                i_WriteData,
    
    // OUTPUTS
	output wire [NB_DATA-1:0]               o_ReadData    
);

    //LOCALPARAMS
    
    //INTERNAL
    reg         [NB_DATA-1:0]        memory[2**RAM_SIZE-1:0];            // Array de registros de 2048 de largo y 16 de ancho
    reg         [NB_DATA-1:0]        tmp_write;
    reg         [NB_DATA-1:0]        tmp_read;
    integer i;

    initial begin
        for (i = 0 ; i < (2**RAM_SIZE) ; i = i+1)
            memory[i] = i;
        memory[8] = 32'hF00F00F0;
    end

    //Storage SB, SH,SW 
    always @(posedge i_clk) begin : write                                     // Realizo la escritura durante el semiciclo positivo
        if (i_MemWrite) begin                                               // del clock y la escritura se encuentre habilitada
            //tmp_write <= memory[i_Addr]; 
           case (i_Long) 
                2'b00 : memory[i_Addr] <= { memory[i_Addr][31:8], i_WriteData[7:0] };
				2'b01 : memory[i_Addr] <= { memory[i_Addr][31:16], i_WriteData[15:0] };
				2'b11 : memory[i_Addr] <= i_WriteData;
				default: memory[i_Addr] <= i_WriteData;
           endcase
           // memory[i_Addr] <= i_WriteData;       
        end                                  
    end
    
    always @(*) begin : read
        if(i_MemRead) begin
            
            tmp_read = memory[i_Addr];                                                //Guardo el dato de 32 bits de la direccion indicada por i_Addr en una variable temporal
            
            if(i_Signed) begin                                                         //Si la senial de signo esta activa tengo que hacer la extension de signo, para saber si es LW,LB o LH.          
                case(i_Long)                
                    2'b00 : tmp_read = { {24{tmp_read[7]}}, tmp_read[7:0] };          //LB 
                    2'b01 : tmp_read = { {16{tmp_read[15]}}, tmp_read[15:0] };        //LH
                    2'b11 : tmp_read = tmp_read;                                      //LW 
                    default: tmp_read =  32'h00000000;                                //Cuando no se especifica i_Long, devuelvo 0
                endcase
           end else begin                                                               //Si la senial de signo NO esta activa, tengo que devolverlo sin signo, entonces relleno con 0
               case(i_Long)
                    2'b00 : tmp_read = { 24'b0, tmp_read[7:0] };                       //LBU
                    2'b01 : tmp_read = { 16'b0, tmp_read[15:0] };                      //LHU
                    2'b11 : tmp_read = tmp_read;                                       //LWU
                    default: tmp_read =  32'h00000000;                                 //Cuando no se especifica i_Long, devuelvo 0
               endcase
           end
        end
    end
    
    // OUTPUT                                                             // El dato se lee al instante y se asigna a la salida
    assign o_ReadData = tmp_read;                                          // De igual modo cuando se escribe se disponibiliza
                                                                          // apenas se escribe (necesario para monociclo)
                                                                          
    initial begin
        $readmemb(DATA_FILE, memory, 0);                                 //Carga la memoria del programa a partir de la ruta especificada por DATA_FILE
        end 
   
endmodule
