`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/05/2021 07:45:17 PM
// Design Name: 
// Module Name: ID_sum
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ID_sum
#(
    parameter               NB_INST         =   32          // Longitud de la direcci??n con signo
)
(
    // INPUTS   
    input wire  [NB_INST-1:0]           	i_a,
    input wire  [NB_INST-1:0]           	i_b,
    
    // OUTPUTS
	output wire [NB_INST-1:0]       		o_add
);
    
    // OUTPUT
    assign o_add = i_a + i_b;
    
endmodule