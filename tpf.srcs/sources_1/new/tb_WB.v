`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/21/2021 09:43:04 AM
// Design Name: 
// Module Name: tb_WB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_WB();

	localparam NB_INST   		= 32;
    localparam NB_TREG      	= 5;
    localparam NB_2          	= 2;

    // SEL MPX MEMTOREG
    localparam ALU 				= 2'b00;
    localparam MEM 				= 2'b01;
    localparam PC 				= 2'b10;

    // TB_SIGNALS
    // INPUTS
    reg                         i_clk;
    reg                         i_reset;
    // MPX
    reg [NB_TREG-1:0]			i_wreg_addr;
    reg                        	i_RegWrite;
    reg [NB_2-1:0]				i_MemtoReg;
    reg [NB_INST-1:0]			i_pc;
    reg [NB_INST-1:0]          	i_DataRead;
    reg [NB_INST-1:0]          	i_ALUResult;

    // OUTPUTS
    wire [NB_TREG-1:0]			o_wreg_addr;
    wire                        o_RegWrite; 
    wire [NB_INST-1:0]			o_wreg_data;

    initial begin
        #10
        i_clk 		= 1'b0;
        i_reset 	= 1'b1;
        
        i_pc 		= 32'b100;
        i_DataRead	= 32'b1000;
        i_ALUResult	= 32'b10000;
        i_wreg_addr = 5'b101;

        i_RegWrite	= 1'b0;

        #10
        i_MemtoReg	= PC;
        i_reset = 1'b0;

       	#20
        // PC
        if ((o_wreg_addr != 32'd5) || (o_RegWrite != 1'b0) || (o_wreg_data != 32'd4)) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end

        // MEM
        i_MemtoReg	= MEM;
    	#20
        if ((o_wreg_addr != 32'd5) || (o_RegWrite != 1'b0) || (o_wreg_data != 32'd8)) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end

        // ALU
        i_MemtoReg	= ALU;
    	#20
        if ((o_wreg_addr != 32'd5) || (o_RegWrite != 1'b0) || (o_wreg_data != 32'd16)) begin
        	$display("############# Test FALLO ############");
            $finish();
    	end
    	
		#100
        
        $display("############# Test OK ############");
        $finish();
    end

    // CLOCK_GENERATION
    always #20 i_clk = ~i_clk;

    WB
    #(
        .NB_INST            (NB_INST),
        .NB_TREG            (NB_TREG),
        .NB_2               (NB_2)
    )
    u_WB
    (
        .i_wreg_addr        (i_wreg_addr),
        .i_RegWrite         (i_RegWrite),
        .i_MemtoReg         (i_MemtoReg),
        .i_pc 	            (i_pc),
        .i_DataRead         (i_DataRead),
        .i_ALUResult        (i_ALUResult),
        .o_wreg_addr		(o_wreg_addr),
        .o_RegWrite 		(o_RegWrite),
        .o_wreg_data		(o_wreg_data)
    );

endmodule
