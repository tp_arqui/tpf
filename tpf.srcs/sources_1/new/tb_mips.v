`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/23/2021 02:28:03 PM
// Design Name: 
// Module Name: tb_mips
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module tb_mips();

	localparam NB_INST		= 	32;     	// Tama??o de instruccion
	localparam NB_ADDR 		= 	26;			// Longitud de la dir. inmediata (J|JAL)
    localparam NB_INMED		=   16;         // Longitud del inmediato
    localparam RAM_SIZE		= 	10;    		// Tamanio de la memoria, 1 MB
    localparam NB_OPCODE	=	6;			// Longitud del OPCODE
    localparam NB_TREG		=   5;	   		// Longitud del campo RS,RT,RD
    localparam NB_OPER		=   4;			// Longitud del operando de ALU
    localparam NB_ALUOP		=	3;			// Longitud del ALUOP
    localparam NB_2         =   2;
	localparam SB_TICK      =   16;
	localparam MEM_SIZE     =   5;
	localparam MEMORY_FILE  =   "arit_logic.mem";
	localparam STEPS        =   5;

	// TB_SIGNALS_inputs
    reg                         clk;
    wire                        i_clk;
    reg                         i_reset;
    // CLK_WIZ
    reg                         clk_reset;
    wire                        locked;
    wire                        o_step;

    // INPUTS
    reg                         step_by_step;
    wire                        o_rx_done;
    wire                        o_tx;
    reg                         i_send_program;
    reg                         i_btn;
    wire [NB_INST-1:0]          instruction;

    wire [NB_INST-1:0]     		o_result;
    wire [NB_INST-1:0]     		address;

	initial begin
        #10
        clk 		        = 1'b0;
        i_reset             = 1'b1;
        clk_reset           = 1'b0;
        i_send_program      = 1'b0;
        step_by_step        = 1'b0;
        i_btn               = 1'b0;

        #10
        clk_reset           = 1'b1;

        while(~locked) begin
            #10
            clk_reset = 1'b0;
        end
       

        #10
        i_reset 	       = 1'b1;
        i_send_program     = 1'b1; //Se pone aca porque empieza a transmitir cuando recibe el alto del reset.

        #30
        i_reset 	       = 1'b0;
        
        #41566600
        i_send_program     = 1'b0;
   
        #60
        i_btn = 1'b1;
            
        #100
        i_btn = 1'b0;
        #60
        i_btn = 1'b1;
            
        #100
        i_btn = 1'b0;
        #60
        i_btn = 1'b1;
            
        #200
        i_btn = 1'b0;
        #120
        i_btn = 1'b1;
            
        #100
        i_btn = 1'b0;
        #60
        i_btn = 1'b1;
            
        #200
        i_btn = 1'b0;
        #120
        i_btn = 1'b1;
            
        #100
        i_btn = 1'b0;
        #60
        i_btn = 1'b1;
            
        #100
        i_btn = 1'b0;
        #120
        i_btn = 1'b1;
            
        #200
        i_btn = 1'b0;
        $display("############# Test OK ############");
        $finish();

    end

    // CLOCK_GENERATION
    always #20 clk = ~clk;

    mips
    #(
    	.NB_INST     		(NB_INST),
       	.NB_ADDR          	(NB_ADDR),
       	.NB_INMED	        (NB_INMED),
       	.RAM_SIZE			(RAM_SIZE),
       	.NB_OPCODE        	(NB_OPCODE),
       	.NB_TREG        	(NB_TREG),
       	.NB_OPER        	(NB_OPER),
       	.NB_ALUOP           (NB_ALUOP),
       	.NB_2           	(NB_2)
    )
    u_mips
    (
    	.i_clk				(i_clk),
    	.i_reset			(i_reset),
    	.i_write			(o_write_mips),
    	.i_enable			(o_step),
    	.i_instruction		(instruction),
    	.i_address		    (address),
    	.o_result			(o_result)
	);
    
    
    tx_program
    #(
        .NB_DATA          (NB_INST),
        .SB_TICK          (SB_TICK),
        .MEM_SIZE         (MEM_SIZE),
        .MEMORY_FILE      (MEMORY_FILE)
    )
    u_tx_program
    (
        .i_clk            (i_clk),
        .i_reset          (i_reset), 
        .i_send_program   (i_send_program),
        .o_tx             (o_tx)
        
    );
    
    rx_program
    #(
        .NB_DATA          (NB_INST),
        .SB_TICK          (SB_TICK),
        .MEM_SIZE         (MEM_SIZE),
        .STEPS            (STEPS)         
    )
    u_rx_program
    (
        .i_clk              (i_clk),
        .i_reset            (i_reset),
        .i_step_by_step     (step_by_step), 
        .i_btn              (i_btn), 
        .i_rx               (o_tx),
        .o_write_mips       (o_write_mips),
        .o_rx_done          (o_rx_done),
        .o_instruction      (instruction),
        .o_address          (address),
        .o_step             (o_step)
        
    );

    clk_wiz_0 uut
    (
        .clk_out1           (i_clk),
        .reset              (clk_reset),
        .locked             (locked),
        .clk_in1            (clk)        
    );

endmodule