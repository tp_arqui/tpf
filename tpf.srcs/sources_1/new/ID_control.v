`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/06/2021 09:14:44 PM
// Design Name: 
// Module Name: ID_control
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ID_control
#(
	parameter				NB_OPCODE		=	6,			// Longitud del OPCODE
	parameter				NB_ALUOP		=	3,			// Longitud del OPCODE de ALU
	parameter				NB_2			= 	2
)
(
	// INPUTS   
    input wire					           	i_reset,
    input wire	[NB_OPCODE-1:0]				i_opcode,

    // OUTPUTS
    // EX
   	output wire	[NB_2-1:0]					o_RegDst,
   	output wire								o_ALUSrc,
   	output wire [NB_ALUOP-1:0]				o_ALUOp,

    // MEM
    output wire								o_MemRead,
    output wire								o_MemWrite,
    output wire 							o_Branch,
    output wire 							o_Branchne,
    output wire 							o_Signed,		// Indica si el LOAD es signed o unsigned
    output wire [NB_2-1:0]					o_Long,			// Indica la longitud de dato leído
    output wire 							o_Jump,			// Indica si se trata de un salto incondicional

    // WB
    output wire 							o_RegWrite,
    output wire [NB_2-1:0]					o_MemtoReg,
    output wire 							o_Halt
);


	// LOCALPARAMETERS
	localparam 						R_format  = 6'b000000;								
	localparam 						lw 		  = 6'b100???;
	localparam 						sw		  = 6'b101???;
	localparam 						branch 	  = 6'b0001??;
	localparam 						jump 	  = 6'b00001?;
	localparam 						inmediate = 6'b001???;
	localparam 						halt 	  = 6'b111111;

	// INTERNAL
	// EX
	reg       	[NB_2-1:0]			RegDst;
	reg       						ALUSrc;
	reg 		[NB_ALUOP-1:0]		ALUOp;
	// MEM
	reg       						MemRead;
	reg       						MemWrite;
	reg       						Branch;
	reg       						Branchne;
	reg       						Jump;
	reg       						Signed;
	reg 		[NB_2-1:0]			Long;
	// WB
	reg       						RegWrite;
	reg       	[NB_2-1:0]			MemtoReg;
	reg 							Halt;

	always @(*) begin : proc_decode
		casez (i_opcode)
			
			R_format :
				begin
					RegDst   = 2'b01;
					ALUOp    = 3'b011;
					ALUSrc   = 1'b0;
					Branch   = 1'b0;
					Branchne = 1'b0;
					Jump 	 = 1'b0;
					MemRead  = 1'b0;
					MemWrite = 1'b0;
					Signed 	 = 1'b?;
					Long 	 = 2'b??;
					RegWrite = 1'b1;
					MemtoReg = 2'b00;
					Halt	 = 1'b0;
				end

			lw :
				begin
					RegDst   = 2'b00;
					ALUOp    = 3'b000;
					ALUSrc   = 1'b1;
					Branch   = 1'b0;
					Branchne = 1'b0;
					Jump 	 = 1'b0;
					MemRead  = 1'b1;
					MemWrite = 1'b0;
					RegWrite = 1'b1;
					MemtoReg = 2'b01;
					Halt	 = 1'b0;

					// Diferencia entre L? y L?U
					Signed = ~i_opcode[2];

					//Diferencia entre LW?, LH? y LB?
					Long = i_opcode[1:0];
				end

			sw :
				begin
					RegDst   = 2'b??;
					ALUOp    = 3'b000;
					ALUSrc   = 1'b1;
					Branch   = 1'b0;
					Branchne = 1'b0;
					Jump 	 = 1'b0;
					MemRead  = 1'b0;
					MemWrite = 1'b1;
					Signed 	 = 1'b?;
					RegWrite = 1'b0;
					MemtoReg = 2'b??;
					Halt	 = 1'b0;

					//Diferencia entre SW, SH y SB
					Long = i_opcode[1:0];
				end

			inmediate :
				begin
					RegDst   = 2'b00;
					ALUOp    = i_opcode[NB_ALUOP-1:0];
					ALUSrc   = 1'b1;
					Branch   = 1'b0;
					Branchne = 1'b0;
					Jump 	 = 1'b0;
					MemRead  = 1'b0;
					MemWrite = 1'b0;
					Signed 	 = 1'b?;
					RegWrite = 1'b1;
					MemtoReg = 2'b00;
					Long 	 = 2'b??;
					Halt	 = 1'b0;
				end

			branch :
				begin
					RegDst   = 2'b??;
					ALUOp    = 3'b001;
					ALUSrc   = 1'b0;
					Branch   = ~i_opcode[0];
					Branchne = i_opcode[0];
					Jump 	 = 1'b0;
					MemRead  = 1'b0;
					MemWrite = 1'b0;
					Signed 	 = 1'b?;
					RegWrite = 1'b0;
					MemtoReg = 2'b??;
					Long 	 = 2'b??;
					Halt	 = 1'b0;
				end

			jump :
				begin
					RegDst   = (i_opcode[0]) ? 2'b10 : 2'b??;	// JAL -> RegDst=10 | J -> RegDst=??
					ALUOp    = 3'b???;
					ALUSrc   = 1'b?;
					Branch   = 1'b0;
					Branchne = 1'b0;
					Jump 	 = 1'b1;
					MemRead  = 1'b?;
					MemWrite = 1'b0;
					Signed 	 = 1'b?;
					RegWrite = (i_opcode[0]) ? 1'b1 : 1'b0;	// JAL -> RegWrite=1 | J -> RegWrite=0
					MemtoReg = (i_opcode[0]) ? 2'b10 : 2'b??;	// JAL -> MemtoReg=10 | J -> MemtoReg=??
					Long 	 = 2'b??;
					Halt	 = 1'b0;
				end

			halt :
				begin
					RegDst   = 2'b01;
					ALUOp    = 3'b011;
					ALUSrc   = 1'b0;
					Branch   = 1'b0;
					Branchne = 1'b0;
					Jump 	 = 1'b0;
					MemRead  = 1'b0;
					MemWrite = 1'b0;
					Signed 	 = 1'b0;
					RegWrite = 1'b0;
					MemtoReg = 2'b00;
					Long 	 = 2'b00;
					Halt	 = 1'b1;
				end

			default :
				begin
					RegDst   = 2'b00;
					ALUOp    = 3'b000;
					ALUSrc   = 1'b0;
					Branch   = 1'b0;
					Branchne = 1'b0;
					Jump 	 = 1'b0;
					MemRead  = 1'b0;
					MemWrite = 1'b0;
					Signed 	 = 1'b0;
					RegWrite = 1'b0;
					MemtoReg = 2'b00;
					Long 	 = 2'b00;
					Halt	 = 1'b0;
				end
		endcase
	end

	// OUTPUT
	assign o_RegDst 	= RegDst;
	assign o_ALUOp 		= ALUOp;
	assign o_ALUSrc 	= ALUSrc;
	assign o_Branch 	= Branch;
	assign o_Branchne 	= Branchne;
	assign o_Jump 		= Jump;
	assign o_MemRead 	= MemRead ;
	assign o_MemWrite 	= MemWrite;
	assign o_Signed 	= Signed;
	assign o_RegWrite 	= RegWrite;
	assign o_MemtoReg 	= MemtoReg;
	assign o_Long 		= Long;
	assign o_Halt 		= Halt;

endmodule