`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/07/2021 07:45:17 PM
// Design Name: 
// Module Name: IF_mux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module IF_sum
#(
    parameter               NB_ADDR         =   32          // Longitud de la direcci??n
)
(
	input wire[NB_ADDR-1:0] i_pc,
	output reg[NB_ADDR-1:0] o_pc_add
);

always @(*) 
begin
	o_pc_add=i_pc+1;  //Suma 1 a la direccion de entrada y lo pone a la salida
end
	
endmodule
